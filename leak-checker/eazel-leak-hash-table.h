/* eazel-leak-hash-table.h - hash table for a leak checking library
   Virtual File System Library

   Copyright (C) 2000 Eazel

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Pavel Cisler <pavel@eazel.com>
*/

#ifndef EAZEL_LEAK_HASH_TABLE_H
#define EAZEL_LEAK_HASH_TABLE_H

typedef struct EazelLeakHashTable EazelLeakHashTable;
typedef struct EazelHashEntry EazelHashEntry;

typedef gboolean (* EazelLeakHashTableFilterFunction) (const EazelLeakAllocationRecord *record, gpointer callback_data);

EazelLeakHashTable *eazel_leak_hash_table_new    (size_t                               initial_size);
void                   eazel_leak_hash_table_free   (EazelLeakHashTable               *hash_table);
gboolean               eazel_leak_hash_table_remove (EazelLeakHashTable               *table,
							unsigned long                        key);
EazelHashEntry *    eazel_leak_hash_table_add    (EazelLeakHashTable               *table,
							unsigned long                        key);
EazelHashEntry *    eazel_leak_hash_table_find   (EazelLeakHashTable               *table,
							unsigned long                        key);
void                   eazel_leak_hash_table_filter (EazelLeakHashTable               *table,
							EazelLeakHashTableFilterFunction  function,
							gpointer                             callback_data);


typedef struct {
	int 				count;
	size_t 				total_size;
	EazelLeakAllocationRecord 	*sample_allocation;
} EazelLeakTableEntry;

typedef struct EazelLeakTable EazelLeakTable;
typedef gboolean (* EazelEachLeakTableFunction) (EazelLeakTableEntry *entry, void *context);

EazelLeakTable *eazel_leak_table_new           (EazelLeakHashTable         *hash_table,
						      int                            stack_grouping_depth);
void               eazel_leak_table_free          (EazelLeakTable             *leak_table);
void               eazel_leak_table_sort_by_count (EazelLeakTable             *leak_table);
void               eazel_leak_table_sort_by_size  (EazelLeakTable             *leak_table);
void               eazel_leak_table_each_item     (EazelLeakTable             *leak_table,
						      EazelEachLeakTableFunction  function,
						      void                          *context);

#endif /* EAZEL_LEAK_HASH_TABLE_H */
