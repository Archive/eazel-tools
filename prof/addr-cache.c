/* addr-cache.c -- cache address->symbol mapping for external processes

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#include "prof.h"
#include "symbols.h"

#include <glib.h>


/* address lookups */

typedef struct cache_entry_struct cache_entry;
struct cache_entry_struct {
    u_long addr;
    char *function;
    char *file;
    u_int line;
};

static inline void
cache_add (prof_process *proc, u_long addr,
	   char *function, char *file, u_int line)
{
    cache_entry *x = g_new (cache_entry, 1);
    x->addr = addr;
    x->function = function;
    x->file = file;
    x->line = line;

    if (proc->address_table == 0)
	proc->address_table = g_hash_table_new (0, 0);

    g_hash_table_insert (proc->address_table, (gpointer) addr, x);
}

static inline cache_entry *
cache_get (prof_process *proc, u_long addr)
{
    if (proc->address_table != 0)
	return g_hash_table_lookup (proc->address_table, (gpointer) addr);
    else
	return 0;
}

gboolean
cached_address_lookup (prof_process *proc, u_long addr,
		       char **function, char **file, u_int *line)
{
    cache_entry *x = cache_get (proc, addr);
    if (x != 0)
    {
	*function = x->function;
	*file = x->file;
	*line = x->line;
	return TRUE;
    }
    else
    {
	char *function_;
	const char *file_;
	u_int line_;
	if (process_find_line (proc, (gpointer) addr,
			       &file_, &function_, &line_))
	{
	    *file = g_strdup (file_);
	    *function = function_;
	    *line = line_;
	    cache_add (proc, addr, *function, *file, *line);
	    return TRUE;
	}
	else
	    return FALSE;
    }
}

void
destroy_address_cache (prof_process *proc)
{
    if (proc->address_table != 0)
    {
	g_hash_table_destroy (proc->address_table);
	proc->address_table = 0;
    }
}
