/* -*- mode: C; c-file-style: "linux" -*- */

/* MemProf -- memory profiler and leak detector
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/*====*/

#include "prof.h"
#include "symbols.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/sysmacros.h>

static const char *program_name = "prof";

static void
bfd_nonfatal (const char *string)
{
    const char *errmsg = bfd_errmsg (bfd_get_error ());

    if (string)
	fprintf (stderr, "%s: %s: %s\n", program_name, string, errmsg);
    else
	fprintf (stderr, "%s: %s\n", program_name, errmsg);
}

static void
bfd_fatal (const char *string)
{
    bfd_nonfatal (string);
    exit (1);
}

static asymbol **
slurp_symtab (bfd *abfd, long *symcount)
{
    asymbol **sy = (asymbol **) NULL;
    long storage;

    if (!(bfd_get_file_flags (abfd) & HAS_SYMS))
    {
	printf ("No symbols in \"%s\".\n", bfd_get_filename (abfd));
	*symcount = 0;
	return NULL;
    }

    storage = bfd_get_symtab_upper_bound (abfd);
    if (storage < 0)
	bfd_fatal (bfd_get_filename (abfd));

    if (storage)
	sy = (asymbol **) malloc (storage);

    *symcount = bfd_canonicalize_symtab (abfd, sy);
    if (*symcount < 0)
	bfd_fatal (bfd_get_filename (abfd));

    if (*symcount <= 0)
    {
	if (sy != NULL)
	{
	    free (sy);
	    sy = NULL;
	}

	storage = bfd_get_dynamic_symtab_upper_bound (abfd);
	if (storage < 0)
	    bfd_fatal (bfd_get_filename (abfd));

	if (storage)
	    sy = (asymbol **) malloc (storage);
    
	*symcount = bfd_canonicalize_dynamic_symtab (abfd, sy);
	if (*symcount < 0)
	    bfd_fatal (bfd_get_filename (abfd));
    }

    if (*symcount == 0)
    {
	if (sy != NULL)
	{
	    free (sy);
	    sy = NULL;
	}

	fprintf (stderr, "%s: %s: No symbols\n",
		 program_name, bfd_get_filename (abfd));
    }

    return sy;
}

static gboolean
read_bfd (process_map *map)
{
    asection *section;

    map->abfd = bfd_openr (map->name, 0);
    if (map->abfd == 0)
    {
	bfd_nonfatal (map->name);
	return FALSE;
    }

    if (!bfd_check_format (map->abfd, bfd_object))
    {
	bfd_nonfatal (bfd_get_filename (map->abfd));
	return FALSE;
    }

    map->syms = slurp_symtab (map->abfd, &map->symcount);
    if (!map->syms)
	return FALSE;

    for (section = map->abfd->sections; section; section = section->next)
    {
	if (strcmp (section->name, ".text") == 0)
	    break;
    }

    if (!section)
    {
	fprintf (stderr, "%s: %s: %s\n", program_name, map->name,
		 ".text section not found");
	return FALSE;
    }

    map->section = section;
    return TRUE;
}

extern char *cplus_demangle (const char *mangled, int options);

#define DMGL_PARAMS     (1 << 0)        /* Include function args */
#define DMGL_ANSI       (1 << 1)        /* Include const, volatile, etc */

static char *
demangle (process_map *map, const char *name)
{
    char *demangled;
	
    if (bfd_get_symbol_leading_char (map->abfd) == *name)
	++name;

    demangled = cplus_demangle (name, DMGL_ANSI | DMGL_PARAMS);
    return demangled ? demangled : strdup (name);
}

static gboolean 
find_line (process_map *map, bfd_vma addr, const char **filename,
	   char **functionname, unsigned int *line)
{
    const char *name;
  
    if (!map->abfd || !map->syms || !map->section)
	return FALSE;                  

    if (bfd_find_nearest_line (map->abfd, map->section, map->syms,
			       addr - map->section->vma,
			       filename, &name, line)
	&& name != NULL)
    {
	*functionname = demangle (map, name);
	return TRUE;
    }
    else
	return FALSE;
}


/* originally from memprof/process.c */

static void
prepare_map (process_map *map)
{
    g_return_if_fail (!map->prepared);
  
    map->prepared = TRUE;
    read_bfd (map);
}

static void
free_map (process_map *map, gpointer unused)
{
    if (map->abfd != 0)
	bfd_close (map->abfd);
    if (map->syms != 0)
	free (map->syms);
    if (map->name != 0)
	g_free (map->name);
    g_free (map);
}

static void
process_free_map_list (prof_process *process)
{
    if (process->map_list)
    {
	g_list_foreach (process->map_list, (GFunc)free_map, 0);
	g_list_free (process->map_list);
	process->map_list = 0;
    }
}

static void
process_read_maps (prof_process *process)
{
    gchar buffer[1024];
    FILE *in;
    gchar perms[26];
    gchar file[256];
    guint start, end, major, minor, inode;
  
    if (process->threads == 0)
	return;

    snprintf (buffer, 1023, "/proc/%d/maps", process->threads->pid);
    in = fopen (buffer, "r");

    process_free_map_list (process);

    if (in == 0)
	return;

    while (fgets (buffer, 1023, in))
    {
	int count = sscanf (buffer, "%x-%x %15s %*x %x:%x %u %255s",
			    &start, &end, perms, &major, &minor, &inode, file);
	if (count >= 6)
	{
	    if (strcmp (perms, "r-xp") == 0)
	    {
		process_map *map;
		dev_t device = makedev(major, minor);

		map = g_new (process_map, 1);
		map->prepared = FALSE;
		map->addr = start;
		map->size = end - start;

		if (count == 7)
		    map->name = g_strdup (file);
		else
		    map->name = g_strdup (locate_inode (device, inode));

		map->abfd = NULL;
		map->section = NULL;
		map->syms = NULL;
		map->symcount = 0;

		map->do_offset = TRUE;

		if (map->name)
		{
		    struct stat stat1, stat2;
		    char *progname = g_strdup_printf ("/proc/%d/exe",
						      process->threads->pid);
		  
		    if (stat (map->name, &stat1) != 0)
		    {
			g_warning ("Cannot stat %s: %s\n", map->name,
				   g_strerror (errno));
		    }
		    else if (stat (progname, &stat2) != 0)
		    {
			g_warning ("Cannot stat %s: %s\n",
				   process->program_name, g_strerror (errno));
		    }
		    else
		    {
			map->do_offset = !(stat1.st_ino == stat2.st_ino &&
					   stat1.st_dev == stat2.st_dev);
		    }

		    g_free (progname);
		}
		    
		process->map_list = g_list_prepend (process->map_list, map);
	    }
	}
    }
    fclose (in);
}

static process_map *
real_locate_map (prof_process *process, u_long addr)
{
    GList *tmp_list = process->map_list;

    while (tmp_list)
    {
	process_map *tmp_map = tmp_list->data;
      
	if ((addr >= tmp_map->addr) && (addr < tmp_map->addr + tmp_map->size))
	    return tmp_map;
	
	tmp_list = tmp_list->next;
    }

    return 0;
}

static process_map *
locate_map (prof_process *process, u_long addr)
{
    process_map *map = real_locate_map (process, addr);
    if (map == 0)
    {
	gpointer page_addr = (gpointer) (addr - addr % profile_page_size);
	if (g_list_find (process->bad_pages, page_addr))
	    return NULL;

	process_read_maps (process);

	map = real_locate_map (process, addr);

	if (map == 0)
	{
	    process->bad_pages = g_list_prepend (process->bad_pages,
						 page_addr);
	    return 0;
	}
    }

    if (!map->prepared)
	prepare_map (map);

    return map;
}

gboolean 
process_find_line (prof_process *process, void *address,
		   const char **filename, char **functionname,
		   unsigned int *line)
{
    process_map *map = locate_map (process, (u_long) address);
    if (map != 0)
    {
	bfd_vma addr = (bfd_vma) address;
	if (map->do_offset)
	    addr -= map->addr;
	return find_line (map, addr, filename, functionname, line);
    }
    else
	return FALSE;
}

void
process_free_bfd_data (prof_process *proc)
{
    process_free_map_list (proc);
    g_list_free (proc->bad_pages);
    proc->bad_pages = 0;
}
