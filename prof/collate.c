/* collate.c -- data massaging

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#include "prof.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct fill_closure {
    prof_collated *coll;
    int index;
};

static void
fill_callback (prof_function *fun, void *data)
{
    struct fill_closure *c = data;
    u_long value = * (u_long *) (((char *)fun) + c->coll->key);
    c->coll->data[c->index].fun = fun;
    c->coll->data[c->index].value = value;
    c->index++;
    c->coll->value_max = MAX (c->coll->value_max, value);
    c->coll->value_min = MIN (c->coll->value_min, value);
}

static int
compare_values (const void *elt1_, const void *elt2_)
{
    const struct prof_collated_value *elt1 = elt1_, *elt2 = elt2_;
    u_long v1 = elt1->value, v2 = elt2->value;
    return (v1 < v2) - (v1 > v2);
}
    
void
collate_profile (prof_process *proc, int key,
		 void (*callback) (prof_collated *, void *), void *data)
{
    prof_collated *coll = alloca (PC_SIZEOF (proc->total_functions));
    struct fill_closure fc;

    coll->proc = proc;
    coll->key = key;
    coll->value_sum = proc->clock;
    coll->value_min = ULONG_MAX;
    coll->value_max = 0;
    coll->ndata = proc->total_functions;

    /* Fill the array with the correct data value. */
    fc.coll = coll;
    fc.index = 0;
    foreach_function (proc, fill_callback, &fc);

    /* Then sort by it. */
    qsort (coll->data, coll->ndata, sizeof (coll->data[0]), compare_values);

    /* Then call the callback. */
    callback (coll, data);
}

static void
print_callback (prof_collated *coll, void *unused)
{
    int i;
    for (i = 0; i < coll->ndata; i++)
    {
	printf ("%-64s %02g%%\n",
		coll->data[i].fun->function,
		((double) coll->data[i].value) / coll->value_sum * 100.0);
    }
}

void
print_profile (prof_process *proc, int key)
{
    collate_profile (proc, key, print_callback, 0);
}
