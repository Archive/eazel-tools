/* consumer.c -- consumer side of the backtrace buffer

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#include "consumer.h"
#include "prof.h"
#include "record.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <glib.h>

#define MAX_SANE_FRAMES 1000

void
fetch_records (consumer *c,
	       int (*callback) (int, void **, void *), void *data)
{
    prof_record *ptr;

    g_return_if_fail (c != 0);
    g_return_if_fail (callback != 0);

again:
    /* Find the next record */
    ptr = (prof_record *) (c->buffer + c->buffer_ptr);

    /* If it's empty, bail */
    if (ptr->magic == PR_EMPTY_MAGIC)
	return;

    /* If it's not full, try to scan for a valid magic number */
    if (ptr->magic != PR_FULL_MAGIC)
    {
	u_long *buffer = (u_long *) c->buffer;
	int mid = (c->buffer_ptr / sizeof (u_long));
	int top = (c->buffer_size / sizeof (u_long));
	int i;
	for (i = mid; i < top; i++)
	{
	    if (buffer[i] == PR_EMPTY_MAGIC)
	    {
		c->buffer_ptr = i * sizeof (u_long);
		return;
	    }
	}
	for (i = 0; i < mid; i++)
	{
	    if (buffer[i] == PR_EMPTY_MAGIC)
	    {
		c->buffer_ptr = i * sizeof (u_long);
		return;
	    }
	}
	fputs ("Circular buffer is fucked; exiting\n", stderr);
	abort ();
    }

    /* So we have a valid record. */
    {
	int go_again;
	prof_record *pr;
	int size = ptr->size;

	/* Lame attempt at checking against record updating while
	   we're still reading. Should really have another magic
	   value to protect against this (does it cost at all?) */
	g_assert (size < MAX_SANE_FRAMES);

        {
	    /* use GCC variable-sized arrays, alloca wouldn't
	       deallocate immediately */

	    char buf[PR_SIZEOF (size)];
	    pr = (prof_record *) buf;
	    memcpy (pr, ptr, PR_SIZEOF (size));

	    go_again = (*callback) (pr->size, pr->frames, data);
	}

	/* Advance our read-pointer to the next record. */
	c->buffer_ptr = ((c->buffer_ptr + PR_SIZEOF (size)) % c->buffer_size);

	if (go_again)
	    goto again;
    }
}

consumer *
make_consumer (char *filename)
{
    consumer *c = g_new0 (consumer, 1);
    struct stat st;

    g_assert (filename != 0);

    if (stat (filename, &st) == 0)
    {
	g_assert (st.st_size % 4 == 0 && st.st_size > PROF_SMALLEST_BUFFER);

	c->buffer_size = st.st_size;
	c->buffer_ptr = 0;
	c->buffer = mmap (0, c->buffer_size * 2, PROT_READ | PROT_WRITE,
			  MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (c->buffer != MAP_FAILED)
	{
	    int fd = open (filename, O_RDONLY);
	    if (fd != -1)
	    {
		if ((mmap (c->buffer, c->buffer_size,
			   PROT_READ, MAP_FIXED | MAP_SHARED
			   | MAP_DENYWRITE, fd, 0) != MAP_FAILED)
		    && (mmap (c->buffer + c->buffer_size,
			      c->buffer_size, PROT_READ,
			      MAP_FIXED | MAP_SHARED | MAP_DENYWRITE,
			      fd, 0) != MAP_FAILED))
		{
		    c->buffer_file = g_strdup (filename);

		    /* unlink the file so it's never left lying around */
		    close (fd);
		    unlink (c->buffer_file);

#ifdef MADV_SEQUENTIAL
		    madvise (c->buffer, c->buffer_size * 2, MADV_SEQUENTIAL);
#endif

		    return c;
		}
		else
		    perror (filename);
		close (fd);
	    }
	    else
		perror (filename);
	    munmap (c->buffer, c->buffer_size * 2);
	}
	else
	    fputs ("Unable to allocate shared buffer.\n", stderr);
	unlink (filename);
    }
    else
	perror (filename);

    g_free (c);
    return 0;
}

void
free_consumer (consumer *c)
{
    g_return_if_fail (c != 0);

    munmap (c->buffer, c->buffer_size);
    munmap (c->buffer + c->buffer_size, c->buffer_size);
    unlink (c->buffer_file);
    free (c->buffer_file);
    g_free (c);
}

static RETSIGTYPE
sigusr1_handler (int unused)
{
    mark_profile_event ();
}

void
init_consumers (void)
{
    struct sigaction sa;
    sa.sa_handler = sigusr1_handler;
    sigemptyset (&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sigaction (SIGUSR1, &sa, 0);
}
