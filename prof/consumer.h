/* consumer.h -- the consumer side of the communication

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#ifndef CONSUMER_H
#define CONSUMER_H

#include "defs.h"

typedef struct consumer_struct consumer;

struct consumer_struct {
    /* This buffer is mapped twice contiguously (read-only) */
    char *buffer;

    size_t buffer_size;
    u_int buffer_ptr;

    char *buffer_file;
};

/* from consumer.c */
extern void fetch_records (consumer *c,
			   int (*callback) (int, void **, void *),
			   void *data);
extern consumer *make_consumer (char *filename);
extern void free_consumer (consumer *c);
extern void init_consumers (void);

#endif /* CONSUMER_H */
