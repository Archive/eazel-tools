/* front.c -- GNOME front end for profiler

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#include "prof.h"
#include "symbols.h"
#include "consumer.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include <gnome.h>
#include <glade/glade.h>

u_long profile_page_size;

static char *glade_file;

static int pipe_fds[2];
static volatile gboolean pipe_data_pending;
static volatile gboolean exit_pending;

static GtkWidget *main_window;
static GtkWidget *main_notebook;
static GtkWidget *follow_fork_item, *follow_exec_item;

typedef struct prof_window_struct prof_window;
struct prof_window_struct {
    prof_process *proc;
    GtkWidget *page;
    char *title;

    GtkWidget *canvas;
    GnomeCanvasItem *canvas_group;

    int timeout_tag;
};

static GSList *window_list;
static prof_window *current_window;

static gboolean profiling_paused = FALSE;
static int collation_type = PROF_COLLATE_ALL;

static gboolean collate_self = FALSE;
static gboolean collate_all  = FALSE;

static void set_status (const char *fmt, ...);
static void create_process_window (prof_process *proc);
static void delete_process_window (prof_window *pw);

void on_run_button_clicked (GtkWidget *button);
void on_stop_button_clicked (GtkWidget *button);
void on_reset_button_clicked (GtkWidget *button);
void on_pause_button_clicked (GtkWidget *button);
void on_restart_button_clicked (GtkWidget *button);
void on_about_activate (GtkWidget *w);
void on_exit_activate (GtkWidget *w);
void on_preferences_activate (GtkWidget *w);
void on_main_notebook_switch_page (GtkNotebook *book,
				   GtkNotebookPage *page, gint page_num);

PangoFontDescription *
font_description (void)
{
    static PangoFontDescription *desc;

    if (desc == NULL && main_window != NULL)
    {
	GtkStyle *style;

	gtk_widget_ensure_style (main_window);
	style = gtk_widget_get_style (main_window);

	desc = pango_font_description_copy (style->font_desc);
    }

    return desc;
}


/* Canvas stuff */

static void
collate_callback (prof_collated *coll, void *data)
{
    prof_process *proc = data;
    prof_window *pw = proc->user_data;
    double multiplier;
    int i;

    if (pw->canvas_group)
	 gtk_object_destroy (GTK_OBJECT (pw->canvas_group));

    pw->canvas_group = gnome_canvas_item_new (gnome_canvas_root
					      (GNOME_CANVAS (pw->canvas)),
					      gnome_canvas_group_get_type (),
					      "x", 0.0, "y", 0.0,
					      NULL);
    if (coll->value_sum == 0 || coll->value_max == 0)
	return;

    multiplier = 200.0 * (coll->value_sum / (double) coll->value_max);

    { int max_percent = (coll->value_max * 100) / coll->value_sum;
      char buf[64];
      sprintf (buf, "%ld %s, %d %s",
	       coll->value_sum,
	       coll->value_sum == 1 ? _("sample") : _("samples"),
	       proc->total_threads,
	       proc->total_threads == 1 ? _("thread") : _("threads"));
      gnome_canvas_item_new (GNOME_CANVAS_GROUP (pw->canvas_group),
			     gnome_canvas_text_get_type (),
			     "text", buf,
			     "x", 100.0,
			     "y", 5.0,
			     "font_desc", font_description (),
			     "anchor", GTK_ANCHOR_NORTH_WEST,
			     "fill_color", "black",
			     NULL);
      sprintf (buf, "%d%%", max_percent);
      gnome_canvas_item_new (GNOME_CANVAS_GROUP (pw->canvas_group),
			     gnome_canvas_text_get_type (),
			     "text", buf,
			     "x", 100.0 + max_percent * multiplier / 100,
			     "y", 5.0,
			     "font_desc", font_description (),
			     "anchor", GTK_ANCHOR_NORTH,
			     "fill_color", "black",
			     NULL); }

    for (i = 0; i < coll->ndata && coll->data[i].value > 0; i++)
    {
	double height = (((double) coll->data[i].value)
			 / coll->value_sum) * multiplier;
	gnome_canvas_item_new (GNOME_CANVAS_GROUP (pw->canvas_group), 
			       gnome_canvas_rect_get_type (),
			       "x1", 100.0,
			       "y1", 20.0 + i * 20,
			       "x2", 100.0 + height,
			       "y2", 35.0 + i * 20,
			       "fill_color",
			       ((collation_type == PROF_COLLATE_SELF)
				? "lightsteelblue" : "darkseagreen3"),
			       "outline_color", "black",
			       NULL);
	gnome_canvas_item_new (GNOME_CANVAS_GROUP (pw->canvas_group),
			       gnome_canvas_text_get_type (),
			       "text", coll->data[i].fun->function,
			       "x", 100.0,
			       "y", 30.0 + i * 20,
			       "font_desc", font_description (),
			       "anchor", GTK_ANCHOR_EAST,
			       "fill_color", "black",
			       "x_offset", -10.0,
			       NULL);
    }

    gnome_canvas_set_scroll_region (GNOME_CANVAS (pw->canvas),
				    0.0, 0.0, 300.0, i * 20.0 + 20.0);
}

static void
collate_and_draw (prof_process *proc, int key)
{
    if (proc->user_data != 0)
	collate_profile (proc, key, collate_callback, proc);
}    


/* libglade callbacks */

static void
run_callback (char *string, gpointer data)
{
    if (string != 0)
    {
	char *argv[64];
	int i = 0;
	char *tok;
	/* strtok () modifies `string', but that's okay in this case */
	for (tok = strtok (string, " \t"); tok != 0; tok = strtok (0, " \t"))
	    argv[i++] = tok;
	if (i > 0)
	{
	    argv[i] = 0;
	    run_process (argv);
	}
	g_free (string);
    }
}

void
on_run_button_clicked (GtkWidget *button)
{
    gnome_request_dialog (FALSE, _("Enter command to profile:"), 0, 4096,
			  run_callback, 0, GTK_WINDOW (main_window));
}

static gboolean
attach_pid_callback (pid_t pid, void *data)
{
    const char *target_name = data;
    char *name;
    gboolean ret;

    name = get_program_name (pid, TRUE);
    if (name == NULL)
	return FALSE;

    ret = strcmp (name, target_name) == 0;

    g_free (name);
    return ret;
}

static void
attach_callback (char *string, gpointer data)
{
    if (string != 0)
    {
	long pid;

	pid = strtol (string, NULL, 10);

	if (pid <= 0)
	{
	    pid = foreach_pid (attach_pid_callback, string);
	}

	if (pid > 0)
	{
	    prof_thread *th;

	    th = attach_to_pid (pid, 0);
	    if (th != NULL)
		create_process_window (th->proc);
	    else
		gnome_error_dialog (_("Unable to attach to process."));
	}
	else
	    gnome_error_dialog (_("Invalid process id string."));

	g_free (string);
    }
}

void
on_attach_button_clicked (GtkWidget *button)
{
    gnome_request_dialog (FALSE, _("Enter pid to attach to:"), 0, 4096,
			  attach_callback, 0, GTK_WINDOW (main_window));
}

void
on_stop_button_clicked (GtkWidget *button)
{
    if (current_window != 0)
	delete_process_window (current_window);
}

void
on_reset_button_clicked (GtkWidget *button)
{
    reset_all_profile_state ();
    set_status (_("Discarded all profile data."));
}

void
on_pause_button_clicked (GtkWidget *button)
{
    if (!profiling_paused)
    {
	profiling_paused = TRUE;
	set_status (_("Profiling paused."));
    }
}

void
on_restart_button_clicked (GtkWidget *button)
{
    if (profiling_paused)
    {
	profiling_paused = FALSE;
	set_status (_("Profiling restarted."));
    }
}

static void
stringify_callback (prof_collated *coll, void *data)
{
    GString *str = data;
    int max_percent, i;

    if (coll->value_sum == 0)
	return;

    switch (collation_type) {
    case PROF_COLLATE_SELF:
        g_string_sprintfa (str, "Per function collation\n");
	break;
    case PROF_COLLATE_ALL:
        g_string_sprintfa (str, "Cumulative collation\n");
	break;
    }

    g_string_sprintfa (str, "\n");

    g_string_sprintfa (str, "%ld %s\n",
		       coll->value_sum,
		       coll->value_sum == 1 ? _("sample") : _("samples"));
    
    max_percent = (coll->value_max * 100) / coll->value_sum;

    g_string_sprintfa (str, "Max Percent: %d%%\n", max_percent);
    
    for (i = 0; i < coll->ndata && coll->data[i].value > 0; i++)
    {
	double percentage = (((double) coll->data[i].value)
			     / coll->value_sum) * 100.0;
	g_string_sprintfa (str, "%-56s : %3.2g%% (%ld)\n",
			   coll->data[i].fun->function,
			   percentage, coll->data[i].value);
    }
}

void
on_save_button_clicked (GtkWidget *button)
{
    FILE *file = stderr;
    GString *str;

    if (current_window == NULL)
	return;

    g_return_if_fail (current_window->proc != NULL);

    str = g_string_new ("");

    collate_profile (current_window->proc,
		     collation_type, stringify_callback, str);

    fprintf (file, "%s\n", str->str);
    g_string_free (str, TRUE);

    fprintf (file, "Call tree:\n");
    print_call_tree (current_window->proc, file);
}

void
on_about_activate (GtkWidget *w)
{
    GladeXML *xml = glade_xml_new (glade_file, "about_dialog", NULL);
    GtkWidget *dialog = glade_xml_get_widget (xml, "about_dialog");
    glade_xml_signal_autoconnect (xml);
    g_object_unref (G_OBJECT (xml));
    gtk_window_set_transient_for (GTK_WINDOW (dialog),
				  GTK_WINDOW (main_window));
    gtk_widget_show (dialog);
}

void
on_exit_activate (GtkWidget *w)
{
    gtk_main_quit ();
}

void
on_preferences_activate (GtkWidget *w)
{
}

void
on_follow_fork_toggle (GtkWidget *item)
{
    follow_fork = GTK_CHECK_MENU_ITEM (item)->active;
}

void
on_follow_exec_toggle (GtkWidget *item)
{
    follow_exec = GTK_CHECK_MENU_ITEM (item)->active;
}

void
on_profile_type_changed (GtkWidget *w, gpointer data)
{
    GSList *node;

    collation_type = (data == 0) ? PROF_COLLATE_SELF : PROF_COLLATE_ALL;

    for (node = window_list; node != NULL; node = node->next)
    {
	prof_window *pw = node->data;

	if (pw->proc != 0 && pw->timeout_tag == 0)
	{
	    /* Wouldn't get updated otherwise. */

	    collate_and_draw (pw->proc, collation_type);
	}
    }
}


/* Window stuff */

void
on_main_notebook_switch_page (GtkNotebook *book,
			      GtkNotebookPage *page, gint page_num)
{
    GtkWidget *child;

    child = gtk_notebook_get_nth_page (book, page_num);

    current_window = gtk_object_get_data (GTK_OBJECT (child), "prof-window");
}

static void
create_main_window (void)
{
    GladeXML *xml = glade_xml_new (glade_file, "main_window", NULL);
    glade_xml_signal_autoconnect (xml);
    main_window = glade_xml_get_widget (xml, "main_window");
    main_notebook = glade_xml_get_widget (xml, "main_notebook");

    follow_fork_item = glade_xml_get_widget (xml, "follow_fork_item");
    follow_exec_item = glade_xml_get_widget (xml, "follow_exec_item");
    gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (follow_fork_item), follow_fork);
    gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (follow_exec_item), follow_exec);
    gtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (follow_fork_item), TRUE);
    gtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (follow_exec_item), TRUE);

#ifndef FOLLOW_FORK_NO_LONGER_BROKEN
    gtk_widget_set_sensitive (follow_fork_item, FALSE);
#endif

    {
	GtkWidget *option_menu
	    = glade_xml_get_widget (xml, "profile_type_menu");
	GtkWidget *menu = gtk_menu_new ();
	int i;
	for (i = 0; i < 2; i++)
	{
	    GtkWidget *item = (gtk_menu_item_new_with_label
			       (i == 0 ? _("Self") : _("All")));
	    gtk_menu_append (GTK_MENU (menu), item);
	    g_signal_connect (G_OBJECT (item), "activate",
			      (GCallback) on_profile_type_changed,
			      GINT_TO_POINTER (i));
	    gtk_widget_show (item);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu),
				     collation_type == PROF_COLLATE_SELF
				     ? 0 : 1);
	gtk_widget_show_all (option_menu);
    }
	
    g_object_unref (G_OBJECT (xml));
}

static gboolean
process_timeout (gpointer data)
{
    collate_and_draw (data, collation_type);
    return TRUE;
}

static char *
process_title (prof_process *proc)
{
    char buf[256];
    char *tem = proc->program_name ? strrchr (proc->program_name, '/') : 0;
    g_assert (proc->threads != 0);
    sprintf (buf, "%s (%d)", tem ? tem + 1: proc->program_name,
	     proc->threads->pid);
    return g_strdup (buf);
}

static void
create_process_window (prof_process *proc)
{
    prof_window *pw = proc->user_data;
    GtkWidget *scroller;

    if (pw != 0)
	return;

    pw = g_new0 (prof_window, 1);
    proc->user_data = pw;
    pw->proc = proc;

    pw->page = gtk_vbox_new (FALSE, 0);
    pw->title = process_title (proc);
    gtk_notebook_append_page (GTK_NOTEBOOK (main_notebook), pw->page,
			      gtk_label_new (pw->title));

    gtk_object_set_data (GTK_OBJECT (pw->page), "prof-window", pw);

    scroller = gtk_scrolled_window_new (0, 0);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller),
				    GTK_POLICY_AUTOMATIC,
				    GTK_POLICY_AUTOMATIC);
    pw->canvas = gnome_canvas_new ();
    gtk_container_add (GTK_CONTAINER (scroller), pw->canvas);
    gtk_box_pack_start (GTK_BOX (pw->page), scroller, TRUE, TRUE, 0);

    pw->timeout_tag = gtk_timeout_add (1000, process_timeout, proc);

    window_list = g_slist_prepend (window_list, pw);

    set_status (_("Connected to %s"), pw->title);

    gtk_widget_show_all (pw->page);
}

static void
delete_process_window (prof_window *pw)
{
    int page = gtk_notebook_page_num (GTK_NOTEBOOK (main_notebook), pw->page);
    if (page >= 0)
	gtk_notebook_remove_page (GTK_NOTEBOOK (main_notebook), page);

    if (pw->page != 0)
	pw->page = 0;

    if (pw->proc != 0)
    {
	pw->proc->user_data = 0;
	release_process (pw->proc);
    }

    window_list = g_slist_remove (window_list, pw);

    if (pw->timeout_tag != 0)
    {
	gtk_timeout_remove (pw->timeout_tag);
	pw->timeout_tag = 0;
    }

    g_free (pw->title);
    g_free (pw);

    if (current_window == pw)
	current_window = 0;
}

#if 0
static void
foreach_window (void (*callback) (prof_window *, void *), void *data)
{
    GSList *x;
    for (x = window_list; x != 0; x = x->next)
	callback (x->data, data);
}
#endif

static void
set_status (const char *fmt, ...)
{
    va_list args;
    char *out;
    va_start (args, fmt);
    vasprintf (&out, fmt, args);
    gnome_appbar_set_status (GNOME_APPBAR (GNOME_APP (main_window)->statusbar), out);
    free (out);
    va_end (args);
}


/* main stuff */

static RETSIGTYPE
sigint_handler (int unused)
{
    /* It's probably not safe to call gtk_main_quit () from a signal
       handler, so use this thing.. */

    exit_pending = TRUE;
    mark_profile_event ();
}

void
record_sample (prof_process *proc, int nframes, void **frames)
{
    if (!profiling_paused)
    {
	_record_sample (proc, nframes, frames);
	proc->clock++;
    }
}

static int
record_callback (int nframes, void **frames, void *data)
{
    record_sample (data, nframes, frames);
    return TRUE;
}

static void
event_callback (prof_process *proc, void *unused)
{
    prof_thread *th;
    for (th = proc->threads; th != 0; th = th->next)
    {
	if (th->buffer != 0)
	    fetch_records (th->buffer, record_callback, proc);
    }
}

static gboolean
reclaim_callback (prof_process *proc, void *data)
{
    prof_window *pw = proc->user_data;
    gboolean delete = FALSE;

    if (pw != 0)
    {
	set_status (_("%s exited."), pw->title);

	if (pw->timeout_tag != 0)
	{
	    gtk_timeout_remove (pw->timeout_tag);
	    pw->timeout_tag = 0;
	}

	/* Windows with only a small number of samples are pretty
	   useless, so just delete them.. */
	if (proc->clock < 5 && current_window != pw)
	{
	    delete_process_window (pw);
	    delete = TRUE;
	}
    }

    return delete;
}

static void
profile_event_handler (void)
{
    int dummy;

    pipe_data_pending = FALSE;
    read (pipe_fds[0], &dummy, sizeof (dummy));

    try_waitpid ();
    reclaim_processes (reclaim_callback, 0);
    foreach_process (event_callback, 0);

    if (exit_pending)
	gtk_main_quit ();
}

/* Called to show that their may be events to handle. This is typically
   called from the SIGUSR1 and SIGCHLD signal handlers. Hence the use
   of a pipe to cause the above function to run.. */
void
mark_profile_event (void)
{
    if (!pipe_data_pending)
    {
	int dummy = 1;
	write (pipe_fds[1], &dummy, sizeof (dummy));
	pipe_data_pending = TRUE;
    }
}

int
main (int argc, char **argv)
{
    static const struct poptOption popt_options [] = {
      { "interval", 0, POPT_ARG_INT, &profile_interval, 0,
	N_("Period between profile sampling (def. 10)"), "MICROSECONDS" },
      { "follow-fork", 0, POPT_ARG_NONE, &follow_fork, 0,
	N_("Profile processes created by fork ()"), NULL },
      { "follow-exec", 0, POPT_ARG_NONE, &follow_exec, 0,
	N_("Keep profiling processes after they call exec ()"), NULL },
      { "collate-self", 0, POPT_ARG_NONE, &collate_self, 0,
	N_("Show each function's time slice"), NULL },
      { "collate-all", 0, POPT_ARG_NONE, &collate_all, 0,
	N_("Show cumulative function time slice"), NULL },
      { NULL, '\0', 0, NULL, 0 },
    };
    poptContext ctx;

    GnomeProgram *program;
    const char **startup_args;

#if 0
    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    textdomain (PACKAGE);
#endif

    program = gnome_program_init ("prof", VERSION,
				  LIBGNOMEUI_MODULE,
				  argc, argv,
				  GNOME_PARAM_POPT_TABLE,
				  popt_options, NULL);

    if (collate_self)
	    collation_type = PROF_COLLATE_SELF;

    if (collate_all)
	    collation_type = PROF_COLLATE_ALL;

    g_object_get (G_OBJECT (program),
		  GNOME_PARAM_POPT_CONTEXT, &ctx,
		  NULL);

    glade_gnome_init ();

    glade_file = "./prof.glade";
    if (!g_file_exists (glade_file))
	glade_file = g_concat_dir_and_file (DATADIR, "prof.glade");
    if (!g_file_exists (glade_file))
	g_error (_("Cannot find prof.glade"));
    
    profile_page_size = getpagesize ();
    init_processes ();
    init_consumers ();
    init_socket ();
    set_create_process_callback (create_process_window);

    create_main_window ();
    gtk_widget_show_all (main_window);

    signal (SIGINT, sigint_handler);
    signal (SIGPIPE, SIG_IGN);

    pipe (pipe_fds);
    gdk_input_add (pipe_fds[0], GDK_INPUT_READ,
		   (GdkInputFunction) profile_event_handler, 0);

    startup_args = poptGetArgs (ctx);
    if (startup_args)
    {
	if (run_process ((char **) startup_args) == -1)
	    exit (1);

    }
    poptFreeContext (ctx);

    gtk_main ();

    delete_socket ();
    kill_processes ();

    return 0;
}
