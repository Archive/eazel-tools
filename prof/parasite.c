/* parasite.c -- the infamous LD_PRELOAD hack

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#define _GNU_SOURCE
#include "defs.h"
#include "stack-frame.h"
#include "record.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/mman.h>
#include <sys/un.h>
#include <stdarg.h>
#include <errno.h>
#include <limits.h>
#include <dlfcn.h>
#include <time.h>
#include <signal.h>
#include <sched.h>
#include <malloc.h>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>

#define TIMER_TYPE ITIMER_PROF

typedef int semaphore;

/* Path to the master socket */
static char *socket_name;

/* Pid of the master process */
static pid_t master_pid;

/* Size of the backtrace buffer */
static size_t buffer_size;

/* Pointers to functions being redefined */
static pid_t (*real_fork) (void);
static int (*real_execve) (const char *, char * const argv[], char * const env[]);
static int (*real_execv) (const char *, char * const argv[]);
static int (*real_execvp) (const char *, char * const argv[]);
static pid_t (*real___clone) (int (*)(void *), void *, int, void *);
static int (*real__exit) (int);

/* Options */
static int follow_fork = 0, follow_exec = 0;
static int profile_interval = 0;

/* Store per-pid data in dynamic structures, since threads share the
   same address space */
typedef struct pid_data_struct pid_data;
struct pid_data_struct {
    pid_data *next;
    pid_t pid;

    int profile_enabled;
    char buffer_file[PATH_MAX];

    /* producer data */
    char *record_buffer;
    size_t record_buffer_size;
    int record_buffer_ptr;
};

/* List of all known pid structures */
static pid_data *pid_list;
static semaphore pid_list_semaphore;

#if 0
# define DB(x) printf x
#else
# define DB(x)
#endif


/* semaphores */

/* Seems we can get livelocked in sem_wait () when using semaphores..? */
#define NO_SEMAPHORES

static inline void
sem_signal (semaphore *sem)
{
#ifndef NO_SEMAPHORES
    *sem = 0;
#endif
}

static inline void
sem_wait (semaphore *sem)
{
#ifndef NO_SEMAPHORES
    int was_blocked;
again:
#if defined (__GNUC__) && defined (__i386__)
    asm ("lock; xchgl %1, %0"
	 : "=r" (was_blocked)
	 : "m" (*sem), "0" (1));
#else
# error "Don't know atomic exchange for your processor"
#endif
    if (was_blocked)
    {
	sched_yield ();
	goto again;
    }
#endif
}


/* Per-pid data. (Needed when CLONE_VM has been given) */

static pid_data *
make_pid_data (pid_t pid)
{
    pid_data *pd = malloc (sizeof (pid_data));

    DB (("creating pid-data for %d\n", pid));

    pd->pid = pid;
    pd->profile_enabled = FALSE;
    tmpnam (pd->buffer_file);
    pd->record_buffer = 0;

    sem_wait (&pid_list_semaphore);
    pd->next = pid_list;
    pid_list = pd;
    sem_signal (&pid_list_semaphore);

    return pd;
}

static void
delete_pid_data (pid_t pid)
{
    pid_data **ptr;
    sem_wait (&pid_list_semaphore);
    for (ptr = &pid_list; *ptr != 0; ptr = &((*ptr)->next))
    {
	pid_data *pd = *ptr;
	if (pd->pid == pid)
	{
	    *ptr = pd->next;
	    free (pd);
	    break;
	}
    }
    sem_signal (&pid_list_semaphore);
}

static pid_data *
get_pid_data (pid_t pid)
{
    pid_data *pd;
    sem_wait (&pid_list_semaphore);
    for (pd = pid_list; pd != 0; pd = pd->next)
    {
	if (pd->pid == pid)
	{
	    sem_signal (&pid_list_semaphore);
	    return pd;
	}
    }
    sem_signal (&pid_list_semaphore);
    return 0;
}


/* socket handling */

static int
sock_write (int fd, void *buf, size_t len)
{
    SOCK_IO (write, fd, buf, len);
}

#if 0
static int
sock_read (int fd, void *buf, size_t len)
{
    SOCK_IO (read, fd, buf, len);
}
#endif

static int
open_socket (void)
{
    int socket_fd;
    struct sockaddr_un addr;

    strcpy (addr.sun_path, socket_name);
    addr.sun_family = AF_UNIX;

    socket_fd = socket (AF_UNIX, SOCK_STREAM, 0);
    if (socket_fd >= 0)
    {
	if (connect (socket_fd, (struct sockaddr *)&addr,
		     sizeof (addr.sun_family)
		     + strlen(addr.sun_path) + 1) == 0)
	{
	    return socket_fd;
	}
	else
	    perror (socket_name);
	close (socket_fd);
    }
    else
	perror ("socket");
    return -1;
}

static int
do_connect (pid_data *pd, pid_t sibling)
{
    int ret = 0;
    int socket = open_socket ();
    if (socket != -1)
    {
	prof_command command;
	prof_connect_command connect;

	command.type = PROF_CONNECT;
	command.size = sizeof (connect);
	command.pid = pd->pid;
	connect.sibling = sibling;
	strcpy (connect.buffer_name, pd->buffer_file);

	if (sock_write (socket, &command, sizeof (command)) == sizeof (command)
	    && sock_write (socket, &connect, sizeof (connect)) == sizeof (connect))
	{
	    ret = 1;
	}
	else
	    perror ("sock_write");
	close (socket);
    }
    return ret;
}


/* producer side of the backtrace buffer */

static void
write_record (pid_data *pd, int nframes, void **frames)
{
    assert (nframes >= 0);
    assert (frames != 0);

    if (pd->record_buffer != 0)
    {
	prof_record *ptr, *next;

	/* Fill in the current (empty) record */
	ptr = (prof_record *) (pd->record_buffer + pd->record_buffer_ptr);
	ptr->size = nframes;
	memcpy (ptr->frames, frames, nframes * sizeof (void *));

	/* Advance the (circular) pointer to the next record.. */
	pd->record_buffer_ptr = ((pd->record_buffer_ptr + PR_SIZEOF (nframes))
				 % pd->record_buffer_size);

	/* ..then mark that record as being empty */
	next = (prof_record *) (pd->record_buffer + pd->record_buffer_ptr);
	next->magic = PR_EMPTY_MAGIC;

	/* Finally, mark the filled-in record as full */
	ptr->magic = PR_FULL_MAGIC;

	assert (master_pid > 0);
	if (kill (master_pid, SIGUSR1) != 0)
	    pd->profile_enabled = FALSE;
    }
}

static int
init_producer_buffer (pid_data *pd)
{
    assert (buffer_size % 4 == 0 && buffer_size > PROF_SMALLEST_BUFFER);

    /* Round BUFFER-SIZE to a multiple of the page size */
    buffer_size = buffer_size & ~ (getpagesize () -1);

    unlink (pd->buffer_file);

    pd->record_buffer_size = buffer_size;
    pd->record_buffer_ptr = 0;
    pd->record_buffer = mmap (0, pd->record_buffer_size * 2,
			      PROT_READ | PROT_WRITE,
			      MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    if (pd->record_buffer != MAP_FAILED)
    {
	int fd = open (pd->buffer_file, O_RDWR | O_CREAT | O_TRUNC, 0666);
	if (fd != -1)
	{
	    if (truncate (pd->buffer_file, buffer_size) == 0)
	    {
		if ((mmap (pd->record_buffer, pd->record_buffer_size,
			   PROT_WRITE, MAP_FIXED | MAP_SHARED, fd, 0)
		     != MAP_FAILED)
		    && (mmap (pd->record_buffer + pd->record_buffer_size,
			      pd->record_buffer_size, PROT_WRITE,
			      MAP_FIXED | MAP_SHARED, fd, 0) != MAP_FAILED))
		{
		    prof_record *pr;

		    close (fd);

		    /* Set the first record empty */
		    pr = (prof_record *) pd->record_buffer;
		    pr->magic = PR_EMPTY_MAGIC;

#ifdef MADV_SEQUENTIAL
		    madvise (pd->record_buffer,
			     pd->record_buffer_size * 2, MADV_SEQUENTIAL);
#endif

		    return TRUE;
		}
		else
		    perror (pd->buffer_file);
	    }
	    else
		perror (pd->buffer_file);
	    close (fd);
	}
	else
	    perror (pd->buffer_file);
	munmap (pd->record_buffer, pd->record_buffer_size * 2);
	pd->record_buffer = 0;
    }
    else
    {
	pd->record_buffer = 0;
	fputs ("Unable to allocate shared buffer.\n", stderr);
    }

    return FALSE;
}

static void
kill_producer_buffer (pid_data *pd, int clone_flags)
{
    if (pd->record_buffer != 0)
    {
	if ((clone_flags & CLONE_VM) == 0)
	{
	    munmap (pd->record_buffer, pd->record_buffer_size);
	    munmap (pd->record_buffer + pd->record_buffer_size,
		    pd->record_buffer_size);
	    pd->record_buffer = 0;
	}
    }
}


/* itimer and SIGPROF handling */

static void
start_timer (void)
{
    struct itimerval it, tem;
    it.it_interval.tv_usec = 0;
    it.it_interval.tv_sec = 0;
    it.it_value.tv_usec = profile_interval % 1000000;
    it.it_value.tv_sec = profile_interval / 1000000;
    setitimer (TIMER_TYPE, &it, &tem);
}

static void
sigprof_callback (int nframes, void **frames, void *data)
{
    write_record (data, nframes, frames);
}

static RETSIGTYPE
sigprof_handler (int unused, struct sigcontext ctx)
{
    int saved_errno = errno;
    pid_data *pd = get_pid_data (getpid ());
    if (pd != 0 && pd->profile_enabled)
    {
	/* Get the real pc of the leaf frame. */
#if defined (__linux__) && defined (__i386__)
	call_with_backtrace ((void *) ctx.eip, (void *) ctx.ebp,
			     (void *) ctx.esp, sigprof_callback, pd);
#elif defined (__linux__) && defined (__powerpc__)
	call_with_backtrace ((void *) ctx.regs->nip, (void *) ctx.regs->gpr[1],
			     NULL, sigprof_callback, pd);
#else
# error "Only works on linux/i386 or linux/powerpc right now"
#endif
	start_timer ();
    }
    errno = saved_errno;
}

static void
init_sighandler (void)
{
    struct sigaction sa;
    sa.sa_handler = (void *) sigprof_handler;
    sigemptyset (&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sigaction (TIMER_TYPE == ITIMER_PROF ? SIGPROF
	       : TIMER_TYPE == ITIMER_VIRTUAL ? SIGVTALRM
	       : SIGALRM, &sa, 0);
}


/* profile initialization */

static pid_data *
init_profiler (pid_t sibling)
{
    pid_data *pd = make_pid_data (getpid ());
    init_sighandler ();
    if (init_producer_buffer (pd) && do_connect (pd, sibling))
    {
	pd->profile_enabled = TRUE;
	start_timer ();
    }
    return pd;
}


/* redefined functions */

pid_t
fork (void)
{
    DB (("fork: %d\n", getpid ()));
#ifdef FOLLOW_FORK_NO_LONGER_BROKEN
    if (socket_name != 0)
    {
	pid_data *parent_pd = get_pid_data (getpid ());
	pid_t pid = (*real_fork) ();

	if (pid == 0)
	{
	    /* child */
#if 0
	    /* XXX this causes crashes? */
	    if (parent_pd != 0)
		kill_producer_buffer (parent_pd, 0);
#endif
	    if (follow_fork)
		init_profiler (0);
	}
	return pid;
    }
    else
#endif
	return (*real_fork) ();
}

struct clone_closure {
    int (*pc) (void *);
    void *data;
    int gc_mark;
    pid_data *parent;
    int flags;
};

static int
clone_callback (void *data)
{
    volatile struct clone_closure *cc = data;
    int (*pc) (void *arg) = cc->pc;
    void *args = cc->data;

    if (cc->parent != 0)
	kill_producer_buffer (cc->parent, cc->flags);

    /* XXX always follow thread creation? */
    init_profiler (cc->parent != 0 ? cc->parent->pid : 0);

    cc->gc_mark = FALSE;
    return (*pc) (args);
}

pid_t
__clone (int (*pc) (void *arg), void *stack, int flags, void *data)
{
    DB (("__clone: (%p, %p, %x, %p) %d\n", pc, stack, flags, data, getpid ()));
    if (socket_name != 0)
    {
	volatile struct clone_closure cc;
	pid_t pid;

	cc.pc = pc;
	cc.data = data;
	cc.gc_mark = TRUE;
	cc.parent = get_pid_data (getpid ());
	cc.flags = flags;

	pid = (*real___clone) (clone_callback, stack, flags, (void *) &cc);

	/* pause until it's safe to free the closure */
	while (cc.gc_mark)
	    sched_yield ();

	return pid;
    }
    else
	return (*real___clone) (pc, stack, flags, data);
}

static void
do_exec ()
{
    DB (("exec* %d\n", getpid ()));
    if (socket_name != 0)
    {
	int socket = open_socket ();
	if (socket != -1)
	{
	    prof_command command;
	    command.type = PROF_WILL_EXEC;
	    command.size = 0;
	    command.pid = getpid ();
	    sock_write (socket, &command, sizeof (command));
	    close (socket);
	}
    }
}

int
execve (const char *filename, char * const argv[], char * const env[])
{
    do_exec ();
    return (*real_execve) (filename, argv, env);
}

int
execv (const char *filename, char * const argv[])
{
    do_exec ();
    return (*real_execv) (filename, argv);
}

int
execvp (const char *filename, char * const argv[])
{
    do_exec ();
    return (*real_execvp) (filename, argv);
}

int
execle (const char *filename, const char *arg, ...)
{
    char *args[1024], *tem;
    char * const *env;
    int i;
    va_list va;

    do_exec ();

    va_start (va, arg);
    i = 0;
    do {
	tem = va_arg (va, char * const );
	args[i++] = tem;
    } while (tem != 0);
    env = va_arg (va, char * const *);
    va_end (va);

    return (*real_execve) (filename, args, env);
}

int
execl (const char *filename, const char *arg, ...)
{
    char *args[1024], *tem;
    int i;
    va_list va;

    do_exec ();

    va_start (va, arg);
    i = 0;
    do {
	tem = va_arg (va, char * const);
	args[i++] = tem;
    } while (tem != 0);
    va_end (va);

    return (*real_execv) (filename, args);
}

int
execlp (const char *filename, const char *arg, ...)
{
    char *args[1024], *tem;
    int i;
    va_list va;

    do_exec ();

    va_start (va, arg);
    i = 0;
    do {
	tem = va_arg (va, char * const);
	args[i++] = tem;
    } while (tem != 0);
    va_end (va);

    return (*real_execvp) (filename, args);
}

void
_exit (int status)
{
    DB (("_exit (%d) %d\n", status, getpid ()));
    if (socket_name != 0)
    {
	pid_data *pd = get_pid_data (getpid ());
	int socket = open_socket ();
	if (socket != -1)
	{
	    prof_command command;
	    command.type = PROF_WILL_EXIT;
	    command.size = 0;
	    command.pid = getpid ();
	    sock_write (socket, &command, sizeof (command));
	    close (socket);
	}
	if (pd != 0)
	{
	    kill_producer_buffer (pd, 0);
	    delete_pid_data (pd->pid);
	}
    }

    (*real__exit) (status);
    abort ();
}


/* This function will be called when the program is initialized
   (before main?). We use this to initialize the profiler (i.e.
   vector into SIGPROF, etc..) */

static void initialize (void) __attribute__ ((constructor));

static u_long
get_numeric_option (char *name, u_long def)
{
    char *tem = getenv (name);
    u_long ret = tem ? strtoul (tem, 0, 0) : def;
    return ret;
}

static void
initialize (void) 
{
    socket_name = getenv ("_PROF_MASTER_SOCK");
    master_pid = get_numeric_option ("_PROF_MASTER_PID", 0);
    buffer_size = get_numeric_option ("_PROF_BUFFER_SIZE", 100 * 1024);
    profile_interval = (int) get_numeric_option ("_PROF_INTERVAL", 10);

    follow_fork = get_numeric_option ("_PROF_FOLLOW_FORK", 0) != 0;
    follow_exec = get_numeric_option ("_PROF_FOLLOW_EXEC", 0) != 0;

    real_fork = dlsym (RTLD_NEXT, "fork");
    real_execve = dlsym (RTLD_NEXT, "execve");
    real_execv = dlsym (RTLD_NEXT, "execv");
    real_execvp = dlsym (RTLD_NEXT, "execvp");
    real___clone = dlsym (RTLD_NEXT, "__clone");
    real__exit = dlsym (RTLD_NEXT, "_exit");

    if (!follow_exec)
    {
	unsetenv ("_PROF_MASTER_SOCK");
	unsetenv ("_PROF_MASTER_PID");
	unsetenv ("_PROF_INTERVAL");
	unsetenv ("LD_PRELOAD");
    }

    if (master_pid != 0 && socket_name != 0 && *socket_name != 0)
	init_profiler (0);
}
