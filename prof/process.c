/* process.c -- process management

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#include "prof.h"
#include "symbols.h"
#include "slave.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <glib.h>

static GSList *process_list;

volatile gboolean dead_processes;

static sigset_t chld_sigset;

gboolean follow_fork = FALSE, follow_exec = FALSE;
int profile_interval = 5;

static char **
get_path (void)
{
    static gboolean read_path = FALSE;
    static char *path[64];
    if (!read_path)
    {
	char *in = getenv ("PATH");
	if (in != 0)
	{
	    int i = 0;
	    char *tok;
	    /* dup this since strtok () modifies its input */
	    in = g_strdup (in);
	    for (tok = strtok (in, ":");
		 tok != 0 && i < 63; tok = strtok (0, ":"))
	    {
		path[i++] = tok;
	    }
	    path[i] = 0;
	}
	read_path = TRUE;
    }

    return path;
}

static char *
search_path (const char *prog)
{
    char **path = get_path ();
    if (path != 0)
    {
	int i;
	for (i = 0; path[i] != 0; i++)
	{
	    char buf[PATH_MAX];
	    strcpy (buf, path[i]);
	    if (buf[strlen (buf) - 1] != '/')
		strcat (buf, "/");
	    strcat (buf, prog);
	    if (access (buf, X_OK) == 0)
		return g_strdup (buf);
	}
    }
    return g_strdup (prog);
}

char *
get_program_name (pid_t pid, gboolean base_only)
{
    char buf[PATH_MAX], name[256], *ptr;
    int len;

    sprintf (name, "/proc/%d/exe", pid);

    len = readlink (name, buf, sizeof (buf));

    if (len == -1)
	return NULL;

    buf[len] = 0;
    ptr = buf;

    if (base_only)
    {
	ptr = strrchr (buf, '/');
	if (ptr != NULL)
	    ptr = ptr + 1;
	else
	    ptr = buf;
    }

    return g_strdup (ptr);
}

pid_t
foreach_pid (gboolean (*callback) (pid_t pid, void *data), void *data)
{
    DIR *dir;
    struct dirent *d;
    pid_t pid;

    dir = opendir ("/proc");
    if (dir == NULL)
	return;

    while ((d = readdir (dir)) != NULL)
    {
	if (d->d_name[0] >= '0' && d->d_name[0] <= '9')
	{
	    char buf[PATH_MAX];

	    /* a process directory. */

	    snprintf (buf, sizeof (buf), "/proc/%s", d->d_name);

	    if (access (buf, R_OK|X_OK) == 0)
	    {
		/* we have permission to read it. */

		pid = atoi (d->d_name);

		if ((*callback) (pid, data))
		    return pid;
	    }
	}
    }

    closedir (dir);
    return 0;
}

pid_t
run_process (char **argv)
{
    pid_t master = getpid ();
    pid_t ret = fork ();

    switch (ret)
    {
	char buf[256];
	char *prog;

    case 0:				/* child */
	setenv ("LD_PRELOAD", LIBDIR "/libprof-parasite.so", 1);
	setenv ("_PROF_MASTER_SOCK", socket_name, 1);
	sprintf (buf, "%d", master);
	setenv ("_PROF_MASTER_PID", buf, 1);
	setenv ("_PROF_FOLLOW_FORK", follow_fork ? "1" : "0", 1);
	setenv ("_PROF_FOLLOW_EXEC", follow_exec ? "1" : "0", 1);
	if (profile_interval > 0)
	{
	    sprintf (buf, "%d", profile_interval);
	    setenv ("_PROF_INTERVAL", buf, 1);
	}

	if (!g_path_is_absolute (argv[0]))
	    prog = search_path (argv[0]);
	else
	    prog = g_strdup (argv[0]);

	execv (prog, argv);

	perror (argv[0]);
	exit (1);

    case -1:				/* error */
	perror ("fork");
	break;

    default:				/* parent */
	break;
    }

    return ret;
}

prof_thread *
connect_to_pid (pid_t pid, pid_t sibling, char *buffer_file)
{
    prof_process *proc = 0;
    prof_thread *th = 0;
    sigset_t old;
    sigprocmask (SIG_BLOCK, &chld_sigset, &old);

    th = get_thread_by_pid (pid);
    if (th == 0 && sibling != 0)
    {
	prof_thread *tem = get_thread_by_pid (sibling);
	if (tem != 0)
	    proc = tem->proc;
    }
    else
	proc = th ? th->proc : 0;
	
    if (proc == 0)
    {
	proc = g_new0 (prof_process, 1);
	process_list = g_slist_prepend (process_list, proc);
    }

    if (th == 0)
    {
	th = g_new0 (prof_thread, 1);
	th->buffer = make_consumer (buffer_file);
	if (th->buffer == 0)
	{
	    th = 0;
	    goto out;
	}

	th->pid = pid;
	th->buffer_file = g_strdup (buffer_file);

	th->next = proc->threads;
	proc->threads = th;
	proc->total_threads++;
	th->proc = proc;
    }

    if (proc->program_name == 0)
	proc->program_name = get_program_name (pid, FALSE);
out:
    sigprocmask (SIG_SETMASK, &old, 0);
    return th;
}

static void
delete_thread (prof_thread *th)
{   
    prof_thread **ptr;
    sigset_t old;
    sigprocmask (SIG_BLOCK, &chld_sigset, &old);

    ptr = &th->proc->threads;
    while (*ptr != 0)
    {
	if (*ptr == th)
	    *ptr = th->next;
	else
	    ptr = &((*ptr)->next);
    }
    th->proc->total_threads--;

    if (th->buffer != 0)
    {
	free_consumer (th->buffer);
	th->buffer = 0;
    }

    if (th->buffer_file != 0)
    {
	g_free (th->buffer_file);
	th->buffer_file = 0;
    }

    if (th->ptraced)
    {
	ptrace (PTRACE_DETACH, th->pid, NULL, NULL);
	th->ptraced = 0;
    }

    if (th->timeout_tag != 0)
    {
	g_source_remove (th->timeout_tag);
	th->timeout_tag = 0;
    }

    g_free (th);

    sigprocmask (SIG_SETMASK, &old, 0);
}

void
disconnect_process (prof_process *proc)
{
    prof_thread *th, *next;
    sigset_t old;
    sigprocmask (SIG_BLOCK, &chld_sigset, &old);

    destroy_address_cache (proc);
    process_free_bfd_data (proc);

    for (th = proc->threads; th != 0; th = next)
    {
	next = th->next;
	delete_thread (th);
    }

    proc->disconnected = 1;

    dead_processes = TRUE;

    sigprocmask (SIG_SETMASK, &old, 0);
}

static void
delete_process (prof_process *proc)
{
    sigset_t old;
    sigprocmask (SIG_BLOCK, &chld_sigset, &old);

    process_list = g_slist_remove (process_list, proc);

    disconnect_process (proc);
    destroy_profile_tree (proc);
    destroy_function_cache (proc);

    if (proc->program_name != 0)
    {
	g_free (proc->program_name);
	proc->program_name = 0;
    }

    g_free (proc);

    sigprocmask (SIG_SETMASK, &old, 0);
}

void
release_process (prof_process *proc)
{
    if (proc->disconnected)
	delete_process (proc);
}

void
mark_thread_exited (prof_thread *th)
{
    th->exited = TRUE;
    dead_processes = TRUE;
}

prof_thread *
attach_to_pid (pid_t pid, pid_t sibling)
{
    prof_process *proc;
    prof_thread *th;
    sigset_t old;

    sigprocmask (SIG_BLOCK, &chld_sigset, &old);

    th = get_thread_by_pid (pid);

    if (th == NULL && sibling != 0)
    {
	th = get_thread_by_pid (sibling);
	if (th != 0)
	    proc = th->proc;
	th = NULL;
    }
    else
	proc = th ? th->proc : NULL;

    if (proc == NULL)
    {
	proc = g_new0 (prof_process, 1);
	process_list = g_slist_prepend (process_list, proc);
    }

    if (th == NULL)
    {
	th = g_new0 (prof_thread, 1);
	th->pid = pid;
	th->next = proc->threads;
	proc->threads = th;
	proc->total_threads++;
	th->proc = proc;

	if (ptrace (PTRACE_ATTACH, pid, NULL, NULL) == 0)
	{
	    th->ptraced = 1;
	}
	else
	{
	    perror ("ptrace PTRACE_ATTACH");
	    delete_thread (th);
	    th = NULL;
	}
    }

    if (proc->program_name == 0)
	proc->program_name = get_program_name (pid, FALSE);

    sigprocmask (SIG_SETMASK, &old, 0);
    return th;
}

static void
ptrace_stack_callback (int n_frames, void **frames, void *data)
{
    prof_thread *th = data;

    record_sample (th->proc, n_frames, frames);
}

static gboolean
interrupt_callback (gpointer data)
{
    prof_thread *th = data;

    kill (th->pid, SIGSTOP);

#if 0
    /* FIXME: could leave the timeout running? */
    th->timeout_tag = 0;
    return FALSE;
#else
    return TRUE;
#endif
}

void
try_waitpid (void)
{
    pid_t pid;
    int status;

    while ((pid = waitpid (-1, &status, WNOHANG | WUNTRACED)) > 0)
    {
	prof_thread *th;
	int sig;

	th = get_thread_by_pid (pid);
	if (th == NULL)
	    continue;

	if (WIFEXITED (status) || WIFSIGNALED (status))
	{
	    mark_thread_exited (th);
	}
	else if (WIFSTOPPED (status) && th->ptraced)
	{
	    call_with_backtrace_ptrace (th->pid, ptrace_stack_callback, th);

	    sig = WSTOPSIG (status);
	    if (sig == SIGSTOP)
		sig = 0;

	    if (ptrace (PTRACE_CONT, th->pid, NULL, (void *) sig) == 0)
	    {
		if (th->timeout_tag == 0)
		    th->timeout_tag = g_timeout_add (profile_interval,
						     interrupt_callback, th);
	    }
	    else
	    {
		delete_thread (th);
	    }
	}
    }
}

static RETSIGTYPE
sigchld_handler (int unused)
{
    mark_profile_event ();
}

void
reclaim_processes (gboolean (*callback) (prof_process *, void *), void *data)
{
    if (dead_processes)
    {
	GSList *todelete = 0;
	GSList *x;
	sigset_t old;

	sigprocmask (SIG_BLOCK, &chld_sigset, &old);

	dead_processes = FALSE;
	for (x = process_list; x != 0; x = x->next)
	{
	    prof_process *proc = x->data;
	    prof_thread *th, *next;

	    for (th = proc->threads; th != 0; th = next)
	    {
		next = th->next;
		if (th->exited)
		    delete_thread (th);
	    }

	    if (proc->threads == 0)
	    {
		if (callback (proc, data))
		    todelete = g_slist_prepend (todelete, proc);

		disconnect_process (proc);
	    }
	}

	for (x = todelete; x != 0; x = x->next)
	    delete_process (x->data);
	g_slist_free (todelete);

	sigprocmask (SIG_SETMASK, &old, 0);
    }
}

prof_thread *
get_thread_by_pid (pid_t pid)
{
    GSList *x;

    /* XXX use a GHashTable here? */

    for (x = process_list; x != 0; x = x->next)
    {
	prof_process *proc = x->data;
	prof_thread *th;
	for (th = proc->threads; th != 0; th = th->next)
	{
	    if (th->pid == pid)
		return th;
	}
    }

    return 0;
}

void
foreach_process (void (*callback) (prof_process *, void *), void *data)
{
    GSList *x;

    for (x = process_list; x != 0; x = x->next)
	callback (x->data, data);
}

int
process_count (void)
{
    return g_slist_length (process_list);
}    

void
init_processes (void)
{
    struct sigaction sa;

    sigemptyset (&chld_sigset);
    sigaddset (&chld_sigset, SIGCHLD);

    sa.sa_handler = sigchld_handler;
    sigemptyset (&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sigaction (SIGCHLD, &sa, 0);
}

void
kill_processes (void)
{
    GSList *x;

    for (x = process_list; x != 0; x = x->next)
	disconnect_process (x->data);
}
