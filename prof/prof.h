/* prof.h -- main header file for the profiler

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#ifndef PROF_H
#define PROF_H

#define _GNU_SOURCE

#include "defs.h"
#include "consumer.h"
#include <stdio.h>
#include <glib.h>
#include <gtk/gtkwidget.h>

typedef struct prof_process_struct prof_process;
typedef struct prof_thread_struct prof_thread;
typedef struct prof_function_struct prof_function;
typedef struct tree_node_struct prof_tree_node;
typedef struct call_tree_node_struct prof_call_tree_node;
typedef struct prof_collated_struct prof_collated;

struct prof_process_struct {
    prof_thread *threads;
    int total_threads;
    char *program_name;

    GList *map_list;
    GList *bad_pages;

    prof_tree_node *tree;
    prof_call_tree_node *call_tree;
    GHashTable *function_hash;
    GHashTable *address_table;
    int total_functions;

    u_long clock;

    u_int disconnected : 1;

    void *user_data;
};

struct prof_thread_struct {
    prof_thread *next;
    prof_process *proc;
    pid_t pid;

    char *buffer_file;
    consumer *buffer;

    guint timeout_tag;

    u_int ptraced :1;
    u_int exited : 1;
};

struct prof_function_struct {
    char *file;
    char *function;
    prof_tree_node *node;
    u_long self, all;
    u_long clock;
};

#define PROF_COLLATE_SELF (G_STRUCT_OFFSET (prof_function, self))
#define PROF_COLLATE_ALL  (G_STRUCT_OFFSET (prof_function, all))

struct prof_collated_value {
    prof_function *fun;
    u_long value;
};

struct prof_collated_struct {
    prof_process *proc;
    u_long value_sum, value_min, value_max;
    int key;
    int ndata;
    struct prof_collated_value data[1];
};

#define PC_SIZEOF(n) 						\
    (sizeof (prof_collated)					\
     + (sizeof (struct prof_collated_value) * ((n) - 1)))


/* prototypes */

/* from front end */
extern void record_sample (prof_process *proc, int count, void **addrs);

/* from master.c */
extern u_long profile_page_size;
extern void mark_profile_event (void);

/* from addr-cache.c */
extern gboolean cached_address_lookup (prof_process *proc, u_long addr,
				       char **function, char **file,
				       u_int *line);
extern void destroy_address_cache (prof_process *proc);

/* from profile-tree.c */
extern void destroy_profile_tree (prof_process *proc);
extern void destroy_function_cache (prof_process *proc);
extern void print_call_tree (prof_process *proc, FILE *fh);
extern void _record_sample (prof_process *proc, int count, void **addrs);
extern void foreach_function (prof_process *proc,
			      void (*callback) (prof_function *, void *),
			      void *data);
extern void reset_profile_state (prof_process *proc);

/* from process.c */
extern volatile gboolean dead_processes;
extern gboolean follow_fork, follow_exec;
extern int profile_interval;
extern char *get_program_name (pid_t pid, gboolean base_only);
extern pid_t foreach_pid (gboolean (*callback) (pid_t pid, void *data),
			  void *data);
extern pid_t run_process (char **argv);
extern prof_thread * connect_to_pid (pid_t pid, pid_t sibling,
				     char *buffer_file);
extern prof_thread *attach_to_pid (pid_t pid, pid_t sibling);
extern void release_process (prof_process *proc);
extern void disconnect_process (prof_process *proc);
extern void mark_thread_exited (prof_thread *th);
extern void try_waitpid (void);
extern void reclaim_processes (gboolean (*callback) (prof_process *, void *),
			       void *data);
extern int process_count (void);
extern prof_thread *get_thread_by_pid (pid_t pid);
extern void foreach_process (void (*callback) (prof_process *, void *),
			     void *data);
extern void init_processes (void);
extern void kill_processes (void);

/* from collate.c */
extern void collate_profile (prof_process *proc, int key,
			     void (*callback) (prof_collated *, void *),
			     void *data);
extern void print_profile (prof_process *proc, int key);
extern void reset_all_profile_state (void);

/* from socket.c */
extern char *socket_name;
extern void set_create_process_callback (void (*fun)(prof_process *));
extern void init_socket (void);
extern void delete_socket (void);

/* from ef_malloc.c */
extern void *ef_malloc (size_t size);
extern void ef_free (void *ptr);


/* misc */

#ifndef G_DISABLE_CHECKS
# ifdef __GNUC__
#  define noncritical_assert(expr)					\
    do {								\
	if (!(expr)) {							\
	    g_log (G_LOG_DOMAIN, G_LOG_LEVEL_CRITICAL,			\
		   "file %s: line %d (%s): assertion `%s' failed.",	\
		   __FILE__, __LINE__, __PRETTY_FUNCTION__, #expr);	\
	}								\
    } while (0)
# else
#  define noncritical_assert(expr)				\
    do {							\
	if (!(expr))) {						\
	    g_log (G_LOG_DOMAIN, G_LOG_LEVEL_CRITICAL,		\
		   "file %s: line %d: assertion `%s' failed.",	\
		   __FILE__, __LINE__, #expr);			\
	}							\
    } while (0)
# endif
#else
# define noncritical_assert(expr) do {} while (0)
#endif

#endif /* !PROF_H */
