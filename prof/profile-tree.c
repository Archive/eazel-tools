/* profile-tree.c -- profiler data structures

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#include "prof.h"
#include <glib.h>
#include <stdlib.h>
#include <stdio.h>

#undef TWO_LEVEL_HASH

typedef struct tree_node_struct tree_node;

struct tree_node_struct {
    tree_node *left, *right, *parent;
    prof_function *function;
    u_long start_addr, end_addr; /* end_addr is not included */
};

typedef struct call_tree_node_struct call_tree_node;

struct call_tree_node_struct {
    GSList *children;
    prof_function *function;
    long total_samples;
    long samples_at_top;
};

static void free_call_tree_node (call_tree_node *cn, gpointer data);


/* tree management */

static void
assert_tree_node_invariants (tree_node *n)
{
    if (n != 0)
    {
	noncritical_assert (n->parent != n && n->left != n && n->right != n);
	noncritical_assert (!n->parent ||
			      (n->parent->left == n && n->parent->start_addr >= n->end_addr ) ||
			      (n->parent->right == n && n->parent->end_addr <= n->start_addr));
	noncritical_assert (n->start_addr <= n->end_addr);
	noncritical_assert (!n->left || n->left->end_addr <= n->start_addr);
	noncritical_assert (!n->right || n->right->start_addr >= n->end_addr);
    }
}

static tree_node *
tree_node_new (prof_function *fun, u_long start_addr, u_long end_addr)
{
    tree_node *x = g_new (tree_node, 1);
    x->left = x->right = x->parent = 0;
    x->function = fun;
    x->start_addr = start_addr;
    x->end_addr = end_addr;
    return x;
}

static void
tree_node_destroy (tree_node *node)
{
    if (node->left != 0)
	tree_node_destroy (node->left);
    if (node->right != 0)
	tree_node_destroy (node->right);
    g_free (node);
}

static tree_node *
insert_node (prof_process *proc, u_long start_addr,
	     u_long end_addr, prof_function *fun)
{
    tree_node *new = tree_node_new (fun, start_addr, end_addr);
    tree_node *n;

    if (proc->tree == 0)
    {
	proc->tree = new;
	return new;
    }

    n = proc->tree;
    while (n != 0)
    {
        if (end_addr <= n->start_addr) /* <= ? */
	{
	    if (n->left == 0 || start_addr >= n->left->end_addr)
	    {
		new->left = n->left;
		new->parent = n;
		if (n->left != 0)
		    n->left->parent = new;
		n->left = new;

		assert_tree_node_invariants (new);
		assert_tree_node_invariants (new->left);
		assert_tree_node_invariants (new->right);
		assert_tree_node_invariants (new->parent);

		return new;
	    }
	    else
		n = n->left;
	}
	else if (start_addr >= n->end_addr)
	{
	    if (n->right == 0 || end_addr <= n->right->start_addr) /* <= ?*/
	    {
		new->right = n->right;
		new->parent = n;
		if (n->right != 0)
		    n->right->parent = new;
		n->right = new;

		assert_tree_node_invariants (new);
		assert_tree_node_invariants (new->left);
		assert_tree_node_invariants (new->right);
		assert_tree_node_invariants (new->parent);

		return new;
	    }
	    else
		n = n->right;
	}
	else
	{
	    g_error ("Trying to insert into an existing range.");
	    abort ();
	}
    }

    assert_tree_node_invariants (new);
    assert_tree_node_invariants (new->left);
    assert_tree_node_invariants (new->right);
    assert_tree_node_invariants (new->parent);

    return new;
}

static tree_node *
find_node (prof_process *proc, u_long addr)
{
    tree_node *n = proc->tree;
    while (n != 0)
    {
	if (addr < n->start_addr)
	    n = n->left;
	else if (addr >= n->end_addr)
	    n = n->right;
	else
	    break;
    }
    return n;
}

static void
grow_node (tree_node *n, u_long addr)
{
    g_return_if_fail (n != 0);
    int s, e;

    s = n->start_addr;
    e = n->end_addr;

    if (addr < n->start_addr)
    {
	g_return_if_fail (!n->left || n->left->end_addr <= addr);
	g_return_if_fail (!n->parent || n->parent->right != n || n->parent->end_addr <= addr);
	n->start_addr = addr;
    }
    else if (addr >= n->end_addr)
    {
	g_return_if_fail (!n->right || n->right->start_addr > addr);
	g_return_if_fail (!n->parent || n->parent->left != n || n->parent->start_addr > addr);
	n->end_addr = addr + 1;
    }

    assert_tree_node_invariants (n);
    assert_tree_node_invariants (n->left);
    assert_tree_node_invariants (n->right);
    assert_tree_node_invariants (n->parent);
}

void
destroy_profile_tree (prof_process *proc)
{
    if (proc->tree != 0)
    {
	tree_node_destroy (proc->tree);
	proc->tree = 0;
    }
    if (proc->call_tree != 0)
    {
	free_call_tree_node (proc->call_tree, 0);
	proc->call_tree = 0;
    }
}


/* function caching */

static inline prof_function *
lookup_function (prof_process *proc, char *file, char *function)
{
    if (file == 0)
	file = "<unknown-file>";

    if (proc->function_hash != 0)
    {
#ifdef TWO_LEVEL_HASH
	GHashTable *indirect = g_hash_table_lookup (proc->function_hash, file);
	if (indirect != 0)
	    return g_hash_table_lookup (indirect, function);
#else
	return g_hash_table_lookup (proc->function_hash, function);
#endif
    }
    return 0;
}

static prof_function *
cache_function_node (prof_process *proc, char *file, char *function)
{
    prof_function *x;

    if (file == 0)
	file = "<unknown-file>";

    x = lookup_function (proc, file, function);
    if (x != 0)
	return x;
    else
    {
	GHashTable *indirect;

	if (proc->function_hash == 0)
	    proc->function_hash = g_hash_table_new (g_str_hash, g_str_equal);

#ifdef TWO_LEVEL_HASH
	indirect = g_hash_table_lookup (proc->function_hash, file);
	if (indirect == 0)
	{
	    indirect = g_hash_table_new (g_str_hash, g_str_equal);
	    g_hash_table_insert (proc->function_hash, file, indirect);
	}
#endif

	x = g_new (prof_function, 1);
	x->file = file;
	x->function = function;
	x->self = x->all = 0;

#ifdef TWO_LEVEL_HASH
	g_hash_table_insert (indirect, function, x);
#else
	g_hash_table_insert (proc->function_hash, function, x);
#endif

	proc->total_functions++;
	return x;
    }
}

static void
free_function_callback (char *key, prof_function *fun, gpointer data)
{
    g_free (fun->function);
    g_free (fun);
}

static void
free_file_callback (char *key, GHashTable *indirect, gpointer data)
{
#ifdef TWO_LEVEL_HASH
    g_hash_table_foreach (indirect, (GHFunc) free_function_callback, 0);
    g_hash_table_destroy (indirect);
#else
    free_function_callback (key, (void *) indirect, data);
#endif
}

void
destroy_function_cache (prof_process *proc)
{
    if (proc->function_hash != 0)
    {
	g_hash_table_foreach (proc->function_hash,
			      (GHFunc) free_file_callback, 0);
	g_hash_table_destroy (proc->function_hash);
	proc->function_hash = 0;
    }
}


/* call tree */

static call_tree_node *
call_tree_node_new (prof_function *function)
{
    call_tree_node *n = g_new0 (call_tree_node, 1);
    n->children = 0;
    n->function = function;
    n->total_samples = n->samples_at_top = 0;
    return n;
}

static call_tree_node *
call_tree_node_ref (call_tree_node *container,
		    prof_function *function, gboolean add)
{
    GSList *x;
    for (x = container->children; x != NULL; x = x->next)
    {
	call_tree_node *node = x->data;
	if (node->function == function)
	    return node;
    }

    if (add)
    {
	call_tree_node *node = call_tree_node_new (function);
	container->children = g_slist_prepend (container->children, node);
	return node;
    }
    else
	return 0;
}

static void
free_call_tree_node (call_tree_node *cn, gpointer data)
{
    g_slist_foreach (cn->children, (GFunc) free_call_tree_node, 0);
    g_slist_free (cn->children);
    g_free (cn);
}

static int
call_tree_sorter (const call_tree_node *n1, const call_tree_node *n2)
{
    return n2->total_samples - n1->total_samples;
}

static void
print_call_tree_1 (FILE *fh, call_tree_node *cn, int depth)
{
    GSList *nodes;
    char *spaces = alloca (depth + 1);
    memset (spaces, ' ', depth);
    spaces[depth] = 0;

    cn->children = g_slist_sort (cn->children,
				 (GCompareFunc) call_tree_sorter);

    for (nodes = cn->children; nodes != 0; nodes = nodes->next)
    {
	call_tree_node *tn = nodes->data;
	if (tn->function != 0)
	{
	    fprintf (fh, "%s %ld %s\n", spaces,
		     tn->total_samples, tn->function->function);
	}
	if (tn->children != 0)
	    print_call_tree_1 (fh, tn, depth + 1);
    }
}

void
print_call_tree (prof_process *proc, FILE *fh)
{
    if (proc->call_tree != 0)
    {
	fprintf (fh, "\nCall tree for process `%s':\n",
		 proc->program_name ? proc->program_name : "<unknown>");

	print_call_tree_1 (fh, proc->call_tree, 0);

	fputc ('\n', fh);
    }
}


/* entry point for recording profile info */

void
_record_sample (prof_process *proc, int count, void **addrs)
{
    int i;
    call_tree_node *container;

    if (proc->call_tree == 0)
	proc->call_tree = call_tree_node_new (0);

    container = proc->call_tree;
    for (i = count - 1; i >= 0; i--)
    {
	prof_function *fun = 0;
	tree_node *tn;
	call_tree_node *cn;

	/* 1. Find the function structure for this address */
	tn = find_node (proc, GPOINTER_TO_UINT (addrs[i]));
	if (tn != 0)
	    fun = tn->function;
	else
	{
	    char *function, *file;
	    u_int line;
	    if (cached_address_lookup (proc, GPOINTER_TO_UINT (addrs[i]),
				       &function, &file, &line))
	    {
		fun = lookup_function (proc, file, function);
		if (fun == 0)
		{
		    fun = cache_function_node (proc, file, function);
		    fun->node = insert_node (proc, GPOINTER_TO_UINT (addrs[i]),
					     GPOINTER_TO_UINT (addrs[i]+1),
					     fun);
		}
		else
		    grow_node (fun->node, GPOINTER_TO_UINT (addrs[i]));
	    }
	}

	/* 2. Update the function's global sample counts */
	if (fun != 0 && fun->clock != proc->clock)
	{
	    if (i == 0)
		fun->self++;
	    fun->all++;
	    fun->clock = proc->clock;
	}

	if (fun != 0)
	{
	    if (strcmp (fun->function, "__libc_start_main") == 0
		|| strcmp (fun->function, "_start") == 0)
	    {
		/* Skip random libc crap */
		continue;
	    }

	    /* 3. Update the call-graph for this function */
	    cn = call_tree_node_ref (container, fun, TRUE);
	    if (cn != NULL)
	    {
		cn->total_samples++;
		if (i == 0)
		    cn->samples_at_top++;
		container = cn;
	    }
	}
    }
}


/* iterating over function records */

struct foreach_closure {
    void (*callback) (prof_function *, void *);
    void *user_data;
};

static void
function_callback (char *name, prof_function *fun,
		   struct foreach_closure *closure)
{
    closure->callback (fun, closure->user_data);
}

static void
file_callback (char *file, GHashTable *indirect,
	       struct foreach_closure *closure)
{
#ifdef TWO_LEVEL_HASH
    g_hash_table_foreach (indirect, (GHFunc) function_callback, closure);
#else
    function_callback (file, (void *) indirect, closure);
#endif
}

void
foreach_function (prof_process *proc,
		  void (*callback) (prof_function *, void *), void *data)
{
    if (proc->function_hash != 0)
    {
	struct foreach_closure closure;
	closure.callback = callback;
	closure.user_data = data;
	g_hash_table_foreach (proc->function_hash,
			      (GHFunc) file_callback, &closure);
    }
}


/* misc functions */

static void
reset_callback (prof_function *fun, void *data)
{
    fun->self = fun->all = 0;
    fun->clock = 0;
}

void
reset_profile_state (prof_process *proc)
{
    foreach_function (proc, reset_callback, 0);
    proc->clock = 0;
}

static void
reset_all_callback (prof_process *proc, void *data)
{
    reset_profile_state (proc);
}

void
reset_all_profile_state (void)
{
    foreach_process (reset_all_callback, 0);
}
