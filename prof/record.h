/* record.h -- the shared memory based communication

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#ifndef RECORD_H
#define RECORD_H

#include "defs.h"
#include <stdio.h>
#include <limits.h>

typedef struct prof_record_struct prof_record;

struct prof_record_struct {
    volatile u_long magic;
    u_long size;
    void *frames[1];
};

#define PR_SIZEOF(n) (sizeof (prof_record) + ((n) - 1) * sizeof (void *))

#define PR_FULL_MAGIC  0xFED00FEDUL
#define PR_EMPTY_MAGIC 0xFEEDFEEDUL

/* XXX random number */
#define PROF_SMALLEST_BUFFER 32768


/* connection to master */

typedef struct prof_command_struct prof_command;

struct prof_command_struct {
    u_long type;
    u_long size;
    pid_t pid;
};

enum prof_commands {
    PROF_CONNECT = 0,
    PROF_WILL_EXEC,
    PROF_WILL_EXIT,
};

typedef struct prof_connect_command_struct prof_connect_command;

struct prof_connect_command_struct {
    pid_t sibling;
    char buffer_name [PATH_MAX];
};

#define SOCK_IO(op, sock, buf, len)			\
	char *buf__ = (char *)buf;			\
	int todo__ = len;				\
	while(todo__ > 0) {				\
	    int this__ = op (sock, buf__, todo__);	\
	    if(this__ < 0) {				\
		if (errno != EINTR)			\
		    return -1;				\
	    }						\
	    else if(this__ == 0)			\
		break;					\
	    else {					\
		todo__ -= this__;			\
		buf__ += this__;			\
	    }						\
	}						\
	return len - todo__;

#endif /* RECORD_H */
