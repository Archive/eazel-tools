/* sampler.c -- text mode front end for profiler

   Copyright (C) 2002 John Harper

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@unfactored.org>  */

#include "prof.h"
#include "symbols.h"
#include "consumer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#ifdef HAVE_GETOPT_H
# include <getopt.h>
#endif

u_long profile_page_size;

static int pipe_fds[2];
static volatile gboolean pipe_data_pending;
static volatile gboolean exit_pending;

static GMainLoop *main_loop;

static int profiling_paused;
static int ready_to_exit;

int max_samples = 1000;

static RETSIGTYPE
sigint_handler (int unused)
{
    /* It's probably not safe to call gtk_main_quit () from a signal
       handler, so use this thing.. */

    exit_pending = TRUE;
    mark_profile_event ();
}

void
record_sample (prof_process *proc, int nframes, void **frames)
{
    if (!profiling_paused)
    {
	_record_sample (proc, nframes, frames);
	proc->clock++;

	if (proc->clock >= max_samples)
	    disconnect_process (proc);
    }
}

static int
record_callback (int nframes, void **frames, void *data)
{
    record_sample (data, nframes, frames);
    return TRUE;
}

static void
event_callback (prof_process *proc, void *unused)
{
    prof_thread *th;
    for (th = proc->threads; th != 0; th = th->next)
    {
	if (th->buffer != 0)
	    fetch_records (th->buffer, record_callback, proc);
    }
}

static gboolean
reclaim_callback (prof_process *proc, void *data)
{
    print_call_tree (proc, stdout);

    if (process_count () == 1)
	exit_pending = TRUE;

    return TRUE;
}

static gboolean
profile_event_handler (void)
{
    int dummy;

    pipe_data_pending = FALSE;
    read (pipe_fds[0], &dummy, sizeof (dummy));

    try_waitpid ();
    reclaim_processes (reclaim_callback, 0);
    foreach_process (event_callback, 0);

    if (exit_pending)
	g_main_loop_quit (main_loop);

    return TRUE;
}

/* Called to show that their may be events to handle. This is typically
   called from the SIGUSR1 and SIGCHLD signal handlers. Hence the use
   of a pipe to cause the above function to run.. */
void
mark_profile_event (void)
{
    if (!pipe_data_pending)
    {
	int dummy = 1;
	write (pipe_fds[1], &dummy, sizeof (dummy));
	pipe_data_pending = TRUE;
    }
}

struct attach_closure {
    const char *name;
    pid_t sibling;
};

static gboolean
attach_to_pid_callback (pid_t pid, void *data)
{
    struct attach_closure *c = data;
    char *name;

    name = get_program_name (pid, TRUE);
    if (name == NULL)
	return FALSE;

    if (strcmp (name, c->name) == 0)
    {
	if (attach_to_pid (pid, c->sibling) != NULL)
	    c->sibling = pid;
    }

    g_free (name);
    return FALSE;
}

static void
usage (const char *prog_name)
{
    fprintf (stderr, "\n\
usage: %s [OPTION]... [PID [SECONDS]]\n\
\n\
  -p, --pid=PROCESS-ID-OR-NAME\n\
  -s, --samples=NUMBER-OF-SAMPLES  (default: 1000)\n\
  -i, --interval=MILLISECONDS      (default: 5)\n\
  -o, --output=FILENAME            (default: stdout)\n\
  -e, --execute=PROGRAM ARGS...\n\
\n\
Samples the call stack of a specified process a given number of times then\n\
prints the call tree history. A command line version of the `prof' program.\n\
\n", prog_name);
}

int
main (int argc, char **argv)
{
    char *pids[32], next_pid = 0;
    gboolean execute_remainder = FALSE;
    int i;
    GIOChannel *channel;

    while (!execute_remainder)
    {
	int c;

	static const char *short_options = "p:en:i:o:";

#ifdef HAVE_GETOPT_LONG
	static const struct option long_options[] = {
	    {"pid", required_argument, 0, 'p'},
	    {"execute", no_argument, 0, 'e'},
	    {"samples", required_argument, 0, 'n'},
	    {"interval", required_argument, 0, 'i'},
	    {"output", required_argument, 0, 'o'},
	    {0}
	};

	int option_index;

	c = getopt_long (argc, argv, short_options,
			 long_options, &option_index);
#else
	c = getopt (argc, argv, short_options);
#endif

	if (c == -1)
	    break;

	switch (c)
	{
	case 'p':
	    if (next_pid >= G_N_ELEMENTS (pids))
	    {
		fprintf (stderr, "Too many --pid arguments\n");
		exit (1);
	    }
	    pids[next_pid++] = strdup (optarg);
	    break;

	case 'e':
	    execute_remainder = TRUE;
	    break;

	case 'n':
	    max_samples = strtol (optarg, NULL, 0);
	    if (max_samples <= 0)
	    {
		usage (argv[0]);
		return 1;
	    }
	    break;

	case 'i':
	    profile_interval = strtol (optarg, NULL, 0);
	    if (profile_interval <= 0)
	    {
		usage (argv[0]);
		return 1;
	    }
	    break;

	case 'o':
	    if (freopen (optarg, "w", stdout) == NULL)
	    {
		perror (optarg);
		exit (1);
	    }
	    break;

	case '?':
#ifndef HAVE_GETOPT_LONG
	    fprintf (stderr, "%s: invalid option -- %c\n", argv[0], optopt);
#endif
	    usage (argv[0]);
	    return 1;
	}
    }

    /* Mimic Mac OS X's `sample PID DURATION' usage. */

    if (optind < argc && next_pid < G_N_ELEMENTS (pids))
    {
	pids[next_pid++] = argv[optind++];

	if (optind < argc)
	{
	    i = atoi (argv[optind++]);
	    if (i > 0)
		max_samples = (i * 1000) / profile_interval;
	}
    }

    if (!execute_remainder && next_pid == 0)
    {
	usage (argv[0]);
	return 0;
    }

    init_processes ();
    init_consumers ();
    init_socket ();

    profile_page_size = getpagesize ();

    main_loop = g_main_loop_new (NULL, FALSE);

    signal (SIGINT, sigint_handler);
    signal (SIGPIPE, SIG_IGN);

    pipe (pipe_fds);

    channel = g_io_channel_unix_new (pipe_fds[0]);
    g_io_add_watch (channel, G_IO_IN, (GIOFunc)
		    profile_event_handler, NULL);

    if (execute_remainder && optind < argc)
    {
	if (run_process ((char **) argv + optind) == -1)
	    exit (1);
    }

    for (i = 0; i < next_pid; i++)
    {
	char *id = pids[i];

	if (id[0] >= '0' && id[0] <= '9')
	{
	    if (attach_to_pid (atoi (id), 0) == NULL)
		exit (1);
	}
	else
	{
	    struct attach_closure c = {0};
	    c.name = id;
	    foreach_pid (attach_to_pid_callback, &c);
	}
    }

    g_main_loop_run (main_loop);

    foreach_process (print_call_tree, stdout);

    delete_socket ();
    kill_processes ();

    return 0;
}
