/* socket.c -- master side of the main socket

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#include "prof.h"
#include "record.h"

#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdarg.h>
#include <errno.h>

#ifdef HAVE_SYS_TIME_H
# include <sys/time.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include <glib.h>
#include <gdk/gdk.h>

/* fd of the socket which clients connect to, or zero. */
static int socket_fd = -1, socket_tag;

/* pathname of the socket. */
char *socket_name;

static GHashTable *input_tags;

static void (*create_process_callback) (prof_process *proc);

#if 0
static int
sock_write (int fd, void *buf, size_t len)
{
    SOCK_IO (write, fd, buf, len);
}
#endif

static int
sock_read (int fd, void *buf, size_t len)
{
    SOCK_IO (read, fd, buf, len);
}

static void
set_fd_nonblocking (int fd)
{
    int flags = fcntl (fd, F_GETFL, 0);
    if (flags != -1)
	fcntl (fd, F_SETFL, flags | O_NONBLOCK);
}

static void
set_fd_blocking (int fd)
{
    int flags = fcntl (fd, F_GETFL, 0);
    if (flags != -1)
	fcntl (fd, F_SETFL, flags & ~O_NONBLOCK);
}

static gboolean
server_handle_request (GIOChannel *c, GIOCondition cond, gpointer data)
{
    int fd;
    prof_command header;

    fd = GPOINTER_TO_UINT (data);

    if (sock_read (fd, &header, sizeof (header)) != sizeof (header))
	goto disconnect;

    switch (header.type)
    {
	prof_connect_command connect;
	prof_thread *th;
	int tag;

    case PROF_CONNECT:
	if (sock_read (fd, &connect, sizeof (connect)) != sizeof (connect))
	    goto disconnect;
	th = connect_to_pid (header.pid, connect.sibling, connect.buffer_name);
	if (th != 0 && create_process_callback != 0)
	    create_process_callback (th->proc);
	break;

    case PROF_WILL_EXEC:
	th = get_thread_by_pid (header.pid);
	if (th != 0)
	    mark_thread_exited (th);
	break;

    case PROF_WILL_EXIT:
	th = get_thread_by_pid (header.pid);
	if (th != 0)
	    mark_thread_exited (th);
	break;

    disconnect:
	tag = (int) g_hash_table_lookup (input_tags, (gpointer) fd);
	g_hash_table_remove (input_tags, (gpointer) fd);
	g_source_remove (tag);
	close (fd);
    }

    return TRUE;
}

static gboolean
server_accept_connection (GIOChannel *c, GIOCondition cond, gpointer data)
{
    int socket_fd, conn_fd;

    socket_fd = GPOINTER_TO_UINT (data);

    conn_fd = accept (socket_fd, NULL, NULL);

    if(conn_fd >= 0)
    {
	GIOChannel *ioc;
	int tag;

	ioc = g_io_channel_unix_new (conn_fd);
	tag = g_io_add_watch (ioc, G_IO_IN | G_IO_HUP | G_IO_ERR,
			      server_handle_request,
			      GUINT_TO_POINTER (conn_fd));
	g_io_channel_unref (ioc);

	g_hash_table_insert (input_tags, (gpointer) conn_fd, (gpointer) tag);

	/* CONN-FD will inherit the properties of SOCKET-FD, i.e. non-
	   blocking. Make it block.. */
	set_fd_blocking (conn_fd);
    }

    return TRUE;
}

void
set_create_process_callback (void (*fun)(prof_process *))
{
    create_process_callback = fun;
}


/* initialisation */

void
init_socket (void)
{
    char namebuf[PATH_MAX];
    char *user;

    input_tags = g_hash_table_new (0, 0);

    user = getenv ("USER");
    if (user == 0)
	user = getenv ("LOGNAME");
    if (user == 0)
	g_error ("You should set $USER or $LOGNAME.");

#ifdef HAVE_SNPRINTF
    snprintf (namebuf, sizeof(namebuf), "/tmp/.prof-%s-%d", user, getpid ());
#else
    sprintf (namebuf, "/tmp/.prof-%s-%d", user, getpid ());
#endif

    /* Delete the socket if it exists */
    if (access (namebuf, F_OK) == 0)
    {
	/* Socket already exists. Delete it */
	unlink (namebuf);

	if (access (namebuf, F_OK) == 0)
	{
	    fprintf (stderr, "Can't delete %s\n", namebuf);
	    return;
	}
    }

    socket_fd = socket (AF_UNIX, SOCK_STREAM, 0);
    if (socket_fd >= 0)
    {
	struct sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	strcpy (addr.sun_path, namebuf);
	if (bind (socket_fd, (struct sockaddr *) &addr,
		sizeof (addr.sun_family) + strlen (addr.sun_path) + 1) == 0)
	{
	    chmod (namebuf, 0700);
	    if (listen (socket_fd, 5) == 0)
	    {
		GIOChannel *ioc;

		set_fd_nonblocking (socket_fd);

		ioc = g_io_channel_unix_new (socket_fd);
		socket_tag = g_io_add_watch (ioc,
					     G_IO_IN | G_IO_HUP | G_IO_ERR,
					     server_accept_connection,
					     GUINT_TO_POINTER (socket_fd));
		g_io_channel_unref (ioc);

		socket_name = g_strdup (namebuf);
		return;
	    }
	    else
		perror ("listen");
	}
	else
	    perror ("bind");
	close (socket_fd);
    }
    else
	perror ("socket");
    socket_fd = -1;

    exit (1);
}

void
delete_socket (void)
{
    if (socket_fd > 0)
    {
	g_source_remove (socket_tag);
	close (socket_fd);
	socket_fd = -1;
	unlink (socket_name);
	socket_name = 0;
    }
}
