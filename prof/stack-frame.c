 /* stack-frame.c -- hacks to read the backtrace

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#include "stack-frame.h"

#include <alloca.h>
#include <string.h>
#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/user.h>

#ifdef HAVE_EXECINFO_H
# include <execinfo.h>
#endif

#if !defined (__GNUC__) || (!defined (__i386__) && !defined (__powerpc__))
# error "Only works on i386/powerpc and gcc right now"
#endif

/* Magic code to chain along the stack frames

   This may even work on non-386 gcc targets, but I haven't tested it.. */

typedef struct stack_frame_struct stack_frame;

struct stack_frame_struct {
    stack_frame *next;
    void *return_address;
};

static inline stack_frame *
first_frame (void)
{
    return __builtin_frame_address (1);
}

static inline stack_frame *
next_frame (stack_frame *fp)
{
    return (fp != 0) ? fp->next : 0;
}

static inline void *
frame_return_address (stack_frame *fp)
{
    return (fp != 0) ? fp->return_address : 0;
}

void
call_with_backtrace (void *first_pc, void *second_fp, void *stack_bound,
		     void (*callback) (int, void **, void *), void *data)
{
    int nframes = 64;
    void **frames = alloca (sizeof (void *) * nframes);
    int depth;
    stack_frame *frame;

    frames[0] = first_pc;

#if defined (__i386__)
    for (frame = second_fp, depth = 1;
	 (void *) frame > stack_bound;
	 frame = next_frame (frame), depth++)
    {                                      
	void *ret = frame_return_address (frame);
	if (depth == nframes)
	{
	    void *new = alloca (sizeof (void *) * (nframes * 2));
	    memcpy (new, frames, nframes * sizeof (void *));
	    frames = new;
	    nframes *= 2;
	}
	frames[depth] = ret;
    }

#else /* defined (__powerpc__) */
    for (frame = second_fp, depth = 0;
	  frame != NULL;
	 frame = next_frame (frame), depth++)
    {                                      
	void *ret = frame_return_address (frame);
	if (depth == nframes)
	{
	    void *new = alloca (sizeof (void *) * (nframes * 2));
	    memcpy (new, frames, nframes * sizeof (void *));
	    frames = new;
	    nframes *= 2;
	}
	if (depth > 0) { 
	    frames[depth] = ret;
	}
    }
#endif

    (*callback) (depth, frames, data);
}

void
call_with_backtrace_ptrace (pid_t pid, void (*callback)
			    (int, void **, void *), void *data)
{
    int nframes = 64;
    void **frames = alloca (sizeof (void *) * nframes);
    int depth;
    stack_frame *fp;

#if defined (__i386__)
    fp = (stack_frame *) ptrace (PTRACE_PEEKUSER, pid,
				 &(((struct user *) 0)->regs.ebp), 0);
    frames[0] = (void *) ptrace (PTRACE_PEEKUSER, pid,
				 &(((struct user *) 0)->regs.eip), 0);

    for (depth = 1; fp != 0; depth++)
    {
	if (depth == nframes)
	{
	    void *new = alloca (sizeof (void *) * (nframes * 2));
	    memcpy (new, frames, nframes * sizeof (void *));
	    frames = new;
	    nframes *= 2;
	}

	frames[depth] = (void *) ptrace (PTRACE_PEEKDATA, pid,
					 &fp->return_address, 0);

	fp = (stack_frame *) ptrace (PTRACE_PEEKDATA, pid, &fp->next, 0);
    }

#else /* defined (__powerpc__) */
    fp = (stack_frame *) ptrace (PTRACE_PEEKUSER, pid,
				 &(((struct user *) 0)->regs.gpr[1]), 0);
    frames[0] = (void *) ptrace (PTRACE_PEEKUSER, pid,
				 &(((struct user *) 0)->regs.nip), 0);

    for (depth = 0; fp != 0; depth++)
    {
	if (depth == nframes)
	{
	    void *new = alloca (sizeof (void *) * (nframes * 2));
	    memcpy (new, frames, nframes * sizeof (void *));
	    frames = new;
	    nframes *= 2;
	}

	if (depth > 0) {
		frames[depth] = (void *) ptrace (PTRACE_PEEKDATA, pid,
						 &fp->return_address, 0);
	}

	fp = (stack_frame *) ptrace (PTRACE_PEEKDATA, pid, &fp->next, 0);
    }

#endif

    (*callback) (depth, frames, data);
}
