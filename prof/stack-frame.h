/* stack-frame.h --

   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: John Harper <jsh@eazel.com>  */

#ifndef STACK_FRAME_H
#define STACK_FRAME_H

#define _GNU_SOURCE

#include "defs.h"

extern void call_with_backtrace (void *first_pc, void *second_fp,
				 void *stack_bound, void (*callback)
				 (int, void **, void *), void *data);

extern void call_with_backtrace_ptrace (pid_t pid, void (*callback)
					(int, void **, void *), void *data);

#endif /* STACK_FRAME_H */
