/* symbols.h -- 

   Copyright (C) 1999 Red Hat, Inc. 
   Copyright (C) 2000 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   $Id$

   Authors: Owen Taylor <otaylor@redhat.com>
            John Harper <jsh@eazel.com>  */

/* this was adapted from memprof */

#ifndef SYMBOLS_H
#define SYMBOLS_H

#define _GNU_SOURCE

#include "prof.h"
#include <sys/types.h>
#include <stdio.h>
#include <glib.h>
#include "bfd.h"


/* bfd magic (from memprof) */

typedef struct process_map_struct process_map;
struct process_map_struct {
  u_int addr;
  u_int size;
  char *name;
  bfd *abfd;
  long symcount;
  asymbol **syms;
  asection *section;
  u_int do_offset : 1;
  u_int prepared : 1;
};

typedef struct Symbol_struct Symbol;
struct Symbol_struct {
  u_long addr;
  u_long size;
  char *name;
};

extern gboolean  process_find_line (prof_process *process, void *address,
				    const char **filename, char **functionname,
				    unsigned int *line);
extern void process_free_bfd_data (prof_process *proc);

/* only for non-2.2+ kernels */
extern char *locate_inode (dev_t device, ino_t inode);
extern void read_inode (const char *path);

#endif /* SYMBOLS_H */
