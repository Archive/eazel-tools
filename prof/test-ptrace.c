
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

typedef struct stack_frame_struct stack_frame;
struct stack_frame_struct {
    stack_frame *next;
    void *return_address;
};

int main (int argc, char **argv)
{
    int pid, samples;
    struct sched_param sp;

    if (argc != 3)
    {
	fprintf (stderr, "usage: %s PID SAMPLES\n", argv[0]);
	return 1;
    }

    pid = atoi (argv[1]);
    samples = atoi (argv[2]);

#if 0
    sp.__sched_priority = 1;
    sched_setscheduler (getpid (), SCHED_FIFO, &sp);
#endif

    if (ptrace (PTRACE_ATTACH, pid, 0, 0) == 0)
    {
	int status;
	int pass;

	for (pass = 0; pass < samples; pass++)
	{
	    stack_frame *fp;
	    long trace[1024];
	    int i, j;

	    if (waitpid (pid, &status, WUNTRACED) == 0)
		status = 0;

	    fp = (stack_frame *) ptrace (PTRACE_PEEKUSER, pid,
					 &(((struct user *) 0)->regs.ebp), 0);
	    trace[0] = ptrace (PTRACE_PEEKUSER, pid,
			       &(((struct user *) 0)->regs.eip), 0);

	    for (i = 0; i < 1024 && fp != 0; i++)
	    {
		trace[i] = ptrace (PTRACE_PEEKDATA, pid,
				   &fp->return_address, 0);
		fp = (stack_frame *) ptrace (PTRACE_PEEKDATA,
					     pid, &fp->next, 0);
	    }

	    for (j = 0; j < i; j++)
	    {
		fprintf (stderr, "0x%lx ", trace[j]);
	    }
	    fputc ('\n', stderr);

	    if (ptrace (PTRACE_CONT, pid, 0, WSTOPSIG (status)) != 0)
		perror ("ptrace PTRACE_CONT");

	    if (1) {
		struct timespec req;
		req.tv_sec = 0;
		req.tv_nsec = 5000000;		/* 5ms */
		nanosleep (&req, 0);
		kill (pid, SIGSTOP);
	    }
	}

	if (ptrace (PTRACE_DETACH, pid, 0, 0) != 0)
	    perror ("ptrace PTRACE_DETACH");
    }

    return 0;
}
