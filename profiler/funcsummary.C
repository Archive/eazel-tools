/*
 * Cprof profiler tool
 * (C) Copyright 1999-2000 Corel Corporation   
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "funcsummary.h"

#include <vector>
#include <algorithm>
#include <functional>
#include <utility>
#include <iomanip>
#include <strstream>

#define DEFAULT_FUNCTION_COLUMN_WIDTH 40

using namespace std;

inline bool 
FuncSummary::sort_func_percent(const FuncData &left,  const FuncData &right)
{
	return right.func_pct < left.func_pct;
}

inline bool 
FuncSummary::sort_func_calls(const FuncData &left, const FuncData &right)
{
	return right.count < left.count;
}

inline bool 
FuncSummary::sort_func_children_percent(const FuncData &left, const FuncData &right)
{
	return right.func_children_pct < left.func_children_pct;
}


inline bool 
FuncSummary::sort_caller_time(const CallerData *const left, const CallerData *const right)
{
	return right->func_time < left->func_time;
}



FuncSummary::FuncSummary(const ProfileData &data, const SymbolTable &syms,
			 sort_order order, profctr_t total_time, bool include_paths)
    :	syms(syms), 
    	order(order),
    	include_paths(include_paths)
{
	time_factor = data.frequency() / 1000;
	
	MyArcVisitor visitor(*this);
	data.VisitArcs(&visitor);
	
	for (function_map_t::iterator i = function_map.begin();
		i != function_map.end(); ++i) {
		i->second.func_pct = (double)i->second.func_time / total_time * 100;
		i->second.func_children_pct = (double)i->second.func_children_time / total_time * 100;
	}
}

FuncSummary::~FuncSummary()
{
}

static const string &
pad_to(string &pad_this, uint pad)
{
        const char spaces[] = "                                                                         ";
	const char *result = pad_this.c_str();
	
	if (pad > pad_this.length()) {
	        uint space_count = pad - pad_this.length();
		if (space_count > sizeof(spaces))
			space_count = sizeof(spaces);
		pad_this += &spaces[sizeof(spaces) - space_count];
		result = pad_this.c_str();
	}
	else {
	  pad_this += "\n";
	  pad_this += &spaces[sizeof(spaces) - DEFAULT_FUNCTION_COLUMN_WIDTH];
	}
	result = pad_this.c_str();
	return pad_this;
}

void 
FuncSummary::OutputCallgraph(ostream &os)
{
	for (function_map_t::iterator i = function_map.begin();
		i != function_map.end(); ++i) {

	  caller_list_t caller_list;
	
	  caller_list.reserve(i->second.callers.size());
	  
	  for (caller_map_t::iterator c = i->second.callers.begin();
	       c != i->second.callers.end(); ++c) {
		  caller_list.push_back(&c->second);
	  }

	  sort(caller_list.begin(), caller_list.end(), sort_caller_time);

	  
	  os << "Function " << i->second.name << endl;
	  os << "=====================" << endl;
	  for (caller_list_t::const_iterator c = caller_list.begin();
	       c != caller_list.end(); ++c) {
		        CallerData *call = *c;
			double time = call->func_time / time_factor;
			double perc = call->func_time / i->second.func_children_time * 100.0;
			
			string padded_name = call->caller->name;
			pad_to(padded_name, 45);

			char buffer[1024];
			snprintf(buffer, sizeof(buffer),
				 "%s%9ld %#7.3f %9.1f",
				 padded_name.c_str(),
				 (long)call->count, perc, time);
			os << buffer << endl;
		}
		os << endl;
		
	}
}

void 
FuncSummary::Output(ostream &os)
{
	typedef std::vector<FuncData> function_list_t;
	function_list_t func_list;
	
	func_list.reserve(function_map.size());
	
	for (function_map_t::const_iterator i = function_map.begin();
		i != function_map.end(); ++i)
		func_list.push_back(i->second);
	
	switch (order) {
		case sort_func:
			sort(func_list.begin(), func_list.end(), sort_func_percent);
			break;
		
		case sort_child:
			sort(func_list.begin(), func_list.end(), sort_func_children_percent);
			break;
		
		case sort_calls:
			sort(func_list.begin(), func_list.end(), sort_func_calls);
			break;
	}

	size_t function_column_width = 16;
	for (function_list_t::const_iterator i = func_list.begin();
		i != func_list.end(); ++i) {
		function_column_width = max (function_column_width, i->name.length());
	}

	if (function_column_width > DEFAULT_FUNCTION_COLUMN_WIDTH) {
	  function_column_width = DEFAULT_FUNCTION_COLUMN_WIDTH;
	}

	string header = "Function Name";
	pad_to(header, function_column_width);
 	header += "    calls   func%  func(ms)    f+c%   f+c(ms)\n";


//	header << /*left() <<*/ setw(max_function_name_length) << "Function name"
//		<< setw(9) << "calls"
//		<< setw(11) << "func %"
//		<< setw(9) << "func (ms)"
//		<< setw(11) << "f+c %"
//		<< setw(11) << "f+c(ms)"
//		<< " path" << ends;

//	os << header.str() << "\n";

	os << header << "\n";

	for (function_list_t::const_iterator i = func_list.begin();
		i != func_list.end(); ++i) {

		if (i->func_pct == 0 || i->count == 0)
			continue;
			
		string padded_name = i->name;
		pad_to(padded_name, function_column_width);

		char buffer[1024];
		snprintf(buffer, sizeof(buffer),
			"%s%9ld %#7.3f %9lld %7.3f %9lld %s", padded_name.c_str(),
			(long)i->count, i->func_pct, i->func_time / time_factor,
			i->func_children_pct, i->func_children_time / time_factor,
			include_paths ? i->path.c_str() : "");

//		ostrstream buffer;

//		buffer << left() << setw(max_function_name_length) << i->name
//			<< setw(9) << i->count
//			<< setw(11) << setprecision(3) << i->func_pct
//			<< setw(9) << i->func_time / time_factor
//			<< setw(11) << setprecision(3) << i->func_children_pct
//			<< setw(11) << i->func_children_time / time_factor
//			<< ends;

//		if (include_paths)
//			buffer << " " << i->path << ends;

//		buffer << ends;

		os << buffer << "\n";
	}
}

FuncSummary::FuncData &FuncSummary::get_func(codeptr_t func)
{
	string name;
	string path;

	syms.Lookup(func, name, path);
	
	function_map_t::iterator i = function_map.find(name);
	if (i != function_map.end()) 
		return i->second;
	
	FuncData f;
	f.name = name;
	f.path = path;
	f.func_time = 0;
	f.func_children_time = 0;
	f.func_pct = 0;
	f.func_children_pct = 0;
	f.count = 0;
	
	return function_map.insert(make_pair(name, f)).first->second;
}

void FuncSummary::MyArcVisitor::visit(const ProfileData::ArcData &a)
{
	FuncData &from = outer.get_func(a.fromfunc);
	FuncData &to = outer.get_func(a.tofunc);
	CallerData *caller;
	
	to.count += a.count;
	
	to.func_time += a.time;
	to.func_children_time += a.time;

	if (from.func_time > 0)
	  	from.func_time -= a.time;

	caller_map_t::iterator i = to.callers.find(&from);
	if (i != to.callers.end()) {
		caller = &i->second;
	} else {
		CallerData d;
		d.caller = &from;
		d.count = 0;
		d.func_time = 0;
		caller = &to.callers.insert(make_pair(&from, d)).first->second;
	}

	caller->count += a.count;
	caller->func_time += a.time;
}


FuncSummary::MyArcVisitor::~MyArcVisitor()
{
}
