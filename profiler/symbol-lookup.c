/*
 * symbol-lookup.c - symbol lookup for a leak checking and profiling
 *  library
 *
 * Copyright (C) 2000 Eazel
 * Copyright (C) 2000 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Authors: Pavel Cisler <pavel@eazel.com>
 *          Alexander Larsson <alexl@redhat.com>
 *
 * This code is partly based on code from MemProf by
 * Owen Taylor, <otaylor@redhat.com>
 */
#define _GNU_SOURCE
	/* need this for dladdr */

#include <bfd.h>
#include <dlfcn.h>
#include <glib.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

typedef struct {
  void *address;
  char *name; /* Demangled */
  asection *section;
  void *section_mapped_at;
} FunctionSymbol;

typedef struct {
  char *object_filename;
  bfd *abfd;
  
  FunctionSymbol *functions; /* Sorted on address */
  int n_functions;

  GHashTable *function_hash; /* Lazily allocated */
  
  asymbol **canonic_symbol_table; /* Lazily allocated */
  
  gulong start;
  gulong end;
} FunctionMapping;

static FunctionMapping *mappings = NULL; /* Sorted on start */
static int n_mappings = 0;

static int compare_functions(const FunctionSymbol *a, const FunctionSymbol *b);
static FunctionMapping *function_mapping_new(const char *object_filename, gboolean executable);
static void function_mapping_get_offsets (FunctionMapping *map);
static gulong filter_functions(FunctionSymbol *out,
			       void *minisyms,
			       unsigned int minisym_size,
			       gulong n_symbols,
			       bfd *abfd,
			       gulong start);
static char *demangle(bfd *abfs, const char *name);
static FunctionMapping *insert_new_mapping(const char *path,
					   gboolean executable);

static int
compare_functions_cover (const void *a, const void *b)
{
	return compare_functions (a, b);
}

static FunctionMapping *
function_mapping_new(const char *object_filename, gboolean executable)
{
  FunctionMapping *map;
  glong n_symbols;
  void *minisyms;
  unsigned int minisym_size;

  map = (FunctionMapping *)g_new(FunctionMapping, 1);
  
  map->abfd = bfd_openr (object_filename, NULL);
  if (map->abfd == NULL) {
    fprintf (stderr, "%s: ", object_filename);
    bfd_perror (object_filename);
    return NULL;
  }
  
  if (!bfd_check_format (map->abfd, bfd_object)) {
    fprintf (stderr, "%s is not an object file\n", object_filename);
    bfd_close (map->abfd);
    return NULL;
  }

  map->object_filename = g_strdup (object_filename);
  map->function_hash = NULL;
  map->canonic_symbol_table = NULL;
  map->start = 0;
  map->end = 0;
  function_mapping_get_offsets(map);
  
  /* Read the symbol table.  */
  n_symbols = bfd_read_minisymbols (map->abfd, 0, &minisyms, &minisym_size);
  if (n_symbols < 0) {
    fprintf (stderr, "%s: ", object_filename);
    bfd_perror (object_filename);
    return NULL;
  }
  if (n_symbols == 0) {
    fprintf (stderr, "no symbols\n");
    bfd_close (map->abfd);
    return NULL;
  }
  
  
  map->functions = g_malloc(sizeof(FunctionSymbol)*n_symbols);
  map->n_functions = filter_functions(map->functions,
				      minisyms,
				      minisym_size,
				      n_symbols,
				      map->abfd,
				      (executable)?0:map->start);
  map->functions = g_realloc(map->functions, map->n_functions*sizeof(FunctionSymbol));
  free(minisyms);
  
  /* Sort the symbol table: */
  qsort(map->functions, map->n_functions, sizeof(FunctionSymbol), compare_functions_cover);
  
  return map;
}

static void
function_mapping_get_offsets (FunctionMapping *map)
{
  gchar buffer[1024];
  FILE *in;
  gchar perms[26];
  gchar file[256];
  unsigned long start, end;
  guint major, minor;
  ino_t inode;
  struct stat library_stat;
  struct stat entry_stat;
  int count;
  
  /* find the library we are looking for in the proc directories
   * to find out at which addresses it is mapped
   */
  snprintf (buffer, 1023, "/proc/%d/maps", getpid());
  in = fopen (buffer, "r");
  
  if (stat (map->object_filename, &library_stat) != 0) {
    /* we will use st_ino and st_dev to do a file match */
    return;
  }
  
  while (fgets(buffer, 1023, in)) {
    gulong tmp;
    
    count = sscanf (buffer, "%lx-%lx %15s %*x %u:%u %lu %255s",
		    &start, &end, perms, &major, &minor, &tmp, file);
    inode = tmp;
    
    if (count >= 6 && strcmp (perms, "r-xp") == 0) {
      if (stat (file, &entry_stat) != 0) {
	break;
      }
      /* check if this is the library we are loading */
      if ((library_stat.st_ino == entry_stat.st_ino) &&
	  (library_stat.st_dev == entry_stat.st_dev)) {
	map->start = start;
	map->end = end;
	break;
      }
    }
  }
  fclose (in);
}

static void
add_symbol(FunctionSymbol *out, asymbol *symbol, bfd *abfd, gulong start)
{
  out->section = bfd_get_section (symbol);
  out->section_mapped_at =
    (void *)(start + (gulong)bfd_section_vma (abfd, out->section));
  out->address = (void *)((gulong)out->section_mapped_at +
			  (gulong)symbol->value);
  out->name = demangle(abfd, symbol->name);
}
	   

static gulong
filter_functions(FunctionSymbol *out,
		 void *minisyms,
		 unsigned int minisym_size,
		 gulong n_symbols,
		 bfd *abfd,
		 gulong start)
{
  bfd_byte *from, *from_end;
  asymbol *store;
  asymbol *symbol;
  gulong copied_symbols;

  store = bfd_make_empty_symbol (abfd);
  if (store == NULL)
    return 0;

  from = (bfd_byte *) minisyms;
  from_end = from + n_symbols * minisym_size;
  copied_symbols = 0;

  while (from < from_end) {
    symbol = bfd_minisymbol_to_symbol (abfd, 0, from, store);
    if (symbol != NULL) {
      if (!bfd_is_und_section (symbol->section) &&
	  (symbol->flags & BSF_FUNCTION) &&
	  !(symbol->flags & BSF_DEBUGGING) &&
	  (symbol->value != 0)) {
	add_symbol(out, symbol, abfd, start);
	out++;
	copied_symbols++;
      }
    }

    from += minisym_size;
  }

  return copied_symbols;
}

/* From libiberty: */
extern char *cplus_demangle (const char *mangled, int options);
#define DMGL_PARAMS     (1 << 0)        /* Include function args */
#define DMGL_ANSI       (1 << 1)        /* Include const, volatile, etc */

static char *
demangle(bfd *abfd, const char *name)
{
	char *demangled;
	
	if (bfd_get_symbol_leading_char (abfd) == *name)
		++name;

	demangled = cplus_demangle (name, DMGL_ANSI | DMGL_PARAMS);
	return (demangled!=NULL) ? demangled : (char *)strdup (name);
}

static int
compare_functions(const FunctionSymbol *a, const FunctionSymbol *b)
{
  if (a->address < b->address)
    return -1;
  if (a->address == b->address)
    return 0;
  return 1;
}

static FunctionMapping *
find_mapping(void *address)
{
  FunctionMapping *map;
  guint l, u, m;
  Dl_info info;

  l = 0;
  u = n_mappings;
  while (l != u) {
    m = (l+u)/2;
    if ((gulong)address < mappings[m].start) {
      u = m;
    } else if (mappings[m].end <= (gulong)address){
      l = m+1;
    } else {
      return &mappings[m];
    }
  }

  /* Not in a known mapping: */
  if (dladdr (address, &info) != 0) {
    map = insert_new_mapping(info.dli_fname, 0);
    return map;
  } 
  return NULL;
}

static FunctionMapping *
insert_new_mapping(const char *path, gboolean executable)
{
  FunctionMapping *map;
  int i;
  
  map = function_mapping_new(path, executable);
  for (i=0;i<n_mappings;i++) {
    if (map->start < mappings[i].start)
      break;
  }
  n_mappings++;
  mappings = g_realloc(mappings, n_mappings*sizeof(FunctionMapping));
  g_memmove(&mappings[i+1], &mappings[i], (n_mappings-1-i)*sizeof(FunctionMapping));
  mappings[i] = *map;
  g_free(map);
  return &mappings[i];
}

static FunctionSymbol *
find_function(FunctionMapping *map, void *address)
{
  int l, u, m;
  
  l = 0;
  u = map->n_functions;
  
  while (l != u) {
    m = (l+u)/2;
    if (address < map->functions[m].address) {
      u = m;
    } else if ((m+1==map->n_functions) ||
	       (address < map->functions[m+1].address)) {
      return &map->functions[m];
    } else {
      l = m+1;
   }
  }
  return &map->functions[0]; /* In front of all symbols */
}

void
init_function_lookup(const char *app_path)
{
  static gboolean initialized = 0;
  
  if (!initialized) {
    insert_new_mapping(app_path, 1);
    initialized = 1;
  }
}

void
get_function_at_address (void *address, char **result)
{
  FunctionMapping *map;
  char *name;

  *result = NULL;

  if (!address)
    return;
  
  map = find_mapping(address);
  if (map == NULL) {
    fprintf (stderr, "Cannot find mapping for address %p", address);
    return;
  }

  if (!map->function_hash) {
    GHashTable *hash;
    int i;

    hash = g_hash_table_new(NULL, NULL);
    for (i=0;i<map->n_functions;i++) {
      g_hash_table_insert(hash,
			  map->functions[i].address,
			  map->functions[i].name);
    }
    map->function_hash = hash;
  } 

  name = g_hash_table_lookup(map->function_hash, address);
  if (name)
    *result = name;
}

void
get_function_near_address (void *address, char **result)
{
  FunctionMapping *map;
  FunctionSymbol *function;

  *result = "unknown symbol";

  if (!address)
    return;
  
  map = find_mapping(address);
  if (map == NULL) {
    fprintf (stderr, "Cannot find mapping for address %p", address);
    return;
  }

  function = find_function(map, address);
  if (function) 
    *result = function->name;
}

void 
get_function_and_file_at_address (void *address, 
				  char **result,
				  char **path,
				  int *line)
{
  FunctionMapping *map;
  FunctionSymbol *function;

  *result = "unknown symbol";
  *path = "";
  *line = 0;

  if (!address)
    return;

  map = find_mapping(address);
  if (map == NULL) {
    fprintf (stderr, "Cannot find mapping for address %p\n", address);
    return;
  }

  function = find_function(map, address);
  *result = function->name;
  
  if (!map->canonic_symbol_table) {
    long symbol_table_size;
    long n_symbols;

    symbol_table_size = bfd_get_symtab_upper_bound (map->abfd);
    if (symbol_table_size < 0) {
      fprintf (stderr, "Symbol table error.\n");
      return;
    }
      
    map->canonic_symbol_table = (asymbol **) malloc (symbol_table_size);
    n_symbols = bfd_canonicalize_symtab (map->abfd,
					 map->canonic_symbol_table);
    if (n_symbols < 0) {
      fprintf (stderr, "No symbols in symbol table\n");
      return;
    }
  }

  if ((!bfd_is_und_section (function->section)) &&
      (function->section->owner == map->abfd)) {
    const char *functionname;
    const char *filename;
    int lineno;

    if ((bfd_find_nearest_line (map->abfd,
				function->section,
				map->canonic_symbol_table,
				(gulong)address - (gulong)function->section_mapped_at,
				&filename,
				&functionname,
				&lineno)) &&
	(filename != NULL) ) {
      *path = (char *)filename;
      *line = lineno;
    }
    if (*path == NULL) {
      *path = map->object_filename;
      *line = -1;
    }
  }
}
