/*
 * symbol-lookup.h - symbol lookup for a leak checking and profiling
 *  library
 *
 * Copyright (C) 2000 Eazel
 * Copyright (C) 2000 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Authors: Pavel Cisler <pavel@eazel.com>
 *          Alexander Larsson <alexl@redhat.com>
 *
 * This code is partly based on code from MemProf by
 * Owen Taylor, <otaylor@redhat.com>
 */

#ifdef __cplusplus
extern "C" {
#endif
  
/* Initialize the symbol lookup library.
 *
 * app_path is the path to the binary. Often /proc/self/exe
 */
void init_function_lookup(const char *app_path);

/* Get the (demangled) name of the function at the exact address
 * specified. The name is placed in *function, and may be NULL
 * if there is no such function.
 *
 * The first time this function is called for a specific object file
 * it allocates an extra hash table.
 *
 * n = number of loaded libraries
 * m = number of functions in the target library
 * Complexity: O(log(n))
 */
void get_function_at_address(const void *address, char **function);

/* Get the (demangled) name of the function nearest to the specified
 * address. Normally the nearest symbol that has lower address.
 * The name is placed in *function, and can be relied on to always
 * be there. If a fatal error occurs the string "unknown symbol" is
 * used.
 *
 * n = number of loaded libraries
 * m = number of functions in the target library
 * Complexity: O(log(n) + log(m))
 */
void get_function_near_address(const void *address, char **function);

/* Get the (demangled) functionname, the source file and line number
 * at the specified address. The function name is always correct, barring
 * fatal errors, but if the source file cannot be found the name of the
 * binary is returned instead, and line_no is -1.
 *
 * The first time this function is called for a specific object file
 * it allocates space for and reads a canonic symbol table from the binary.
 *
 * n = number of loaded libraries
 * m = number of functions in the target library
 * Complexity: O(log(n) + log(m)) + O(unknown bfd call)
 */
void get_function_and_file_at_address(const void *address, 
				      char **function,
				      char **path,
				      int *line_no);

#ifdef __cplusplus
}
#endif
